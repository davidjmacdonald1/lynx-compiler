lexer grammar LynxLexer;

PRINT : 'print' ;

CONST : 'const' ;
VAR : 'var' ;
VAL : 'val' ;
ASSIGN : '=' ;

ELSE : 'else' ;
IF : 'if' ;
OR : 'or' ;
AND : 'and' ;
NOT : 'not' ;
LT : '<' ;
GT : '>' ;
LTE : '<=' ;
GTE : '>=' ;
EQ : '==' ;
NE : '!=' ;
BITOR : '|' ;
BITXOR : '~' ;
BITAND: '&' ;
LSHIFT : '<<' ;
RSHIFT : '>>' ;
PERCENT : '%' ;
SLASH : '/' ;
STAR : '*' ;
CARET : '^' ;
PLUS : '+' ;
MINUS : '-' ;
DOT : '.' ;

LPAREN : '(' ;
RPAREN : ')' ;
LBRACE : '{' ;
RBRACE : '}' ;
COMMA : ',' ;
SEMICOLON : ';' ;
NL : ([\r]? [\n])+ ;
IGNORED : ([ \t] | '/*' .*? '*/' | '//' ~[\n]*) -> skip ;

NUM : DEC_NUM
    | BIN_NUM
    | OCT_NUM
    | HEX_NUM
    ;

fragment DEC_NUM : DEC_SEG
                 | [0-9_]* '.' DEC_SEG
                 | DEC_SEG '.' [0-9_]*
                 ;

fragment DEC_SEG : [0-9] [0-9_]* ;

fragment BIN_NUM : '0b' BIN_SEG ;
fragment BIN_SEG : [0-1_]* [0-1] [0-1_]* ;

fragment OCT_NUM : '0o' OCT_SEG ;
fragment OCT_SEG : [0-7_]* [0-7] [0-7_]* ;

fragment HEX_NUM : '0x' HEX_SEG ;
fragment HEX_SEG : (HEX_DIGIT | '_')* HEX_DIGIT (HEX_DIGIT | '_')* ;
fragment HEX_DIGIT : [0-9a-fA-F] ;

ID : [_a-zA-Z][_a-zA-Z0-9]* ;
