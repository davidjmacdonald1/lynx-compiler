package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func TestIdExpr(t *testing.T) {
	testExpr(t, "true", typ.UntypedBool, "true")
	testExpr(t, "false", typ.UntypedBool, "false")
	testCheckerErr(t, "Int")
	testCheckerErr(t, "Bool")
	testCheckerErr(t, "someVar")

	testExpr(t, "const x = 1.2; x", typ.UntypedFloat, "1.2")
	testExpr(t, "val x = 5; x", typ.Int, "lynx_x")
	testExpr(t, "var x = 5; x", typ.Int, "lynx_x")

}
