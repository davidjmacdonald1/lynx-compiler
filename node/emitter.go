package node

import (
	"fmt"
	"strings"

	"gitlab.com/davidjmacdonald1/lynx-compiler/scope"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

type emitter struct {
	includes map[string]struct{}
	decls    map[string]struct{}
	Scope    scope.Scope
	Depth    int
}

func newEmitter() *emitter {
	e := &emitter{
		includes: make(map[string]struct{}),
		decls:    make(map[string]struct{}),
		Depth:    1,
	}
	e.Include("inttypes")
	e.Include("stdint")
	e.Include("stdbool")
	return e
}

func (e *emitter) Tab() string {
	return strings.Repeat("    ", e.Depth)
}

func (e *emitter) Include(name string) {
	e.includes[name] = struct{}{}
}

func (e *emitter) Declare(decl string) {
	e.decls[decl] = struct{}{}
}

func (e *emitter) DeclareTupleType(t typ.TupleType) {
	b := strings.Builder{}
	b.WriteString("typedef struct {\n")
	for i, member := range t.Members {
		fmt.Fprintf(&b, "    %s x%d;\n", member.Emit(), i)
	}
	fmt.Fprintf(&b, "} %s;\n", t.Emit())
	e.Declare(b.String())
}

func (e *emitter) Header() string {
	b := strings.Builder{}
	for key := range e.includes {
		fmt.Fprintf(&b, "#include <%s.h>\n", key)
	}

	b.WriteString("\n")
	for key := range e.decls {
		fmt.Fprintf(&b, "%s\n", key)
	}
	return b.String()
}

func (e *emitter) LynxName(name string) string {
	return "lynx_" + name
}

func (e *emitter) PushScope(scope scope.Scope) {
	e.Scope = scope
	e.Depth++
}

func (e *emitter) PopScope() {
	e.Scope = e.Scope.Parent()
	e.Depth--
}
