package node

import (
	"fmt"
	"strings"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type TupleExpr struct {
	aNode
	IsVars []bool
	Exprs  []Node
}

func NewTupleExpr(isVars []bool, exprs []Node) *TupleExpr {
	return &TupleExpr{IsVars: isVars, Exprs: exprs}
}

func (node *TupleExpr) Check(c *checker) error {
	if c.TypeCount > 0 {
		return node.checkType(c)
	} else {
		return node.checkExpr(c)
	}
}

func (node *TupleExpr) checkType(c *checker) error {
	members := make([]typ.Type, len(node.Exprs))
	for i, expr := range node.Exprs {
		t, err := c.checkType(expr)
		if err != nil {
			return err
		}
		members[i] = typ.DeclType{IsVar: node.IsVars[i], Type: t}
	}
	node.SetType(typ.Type_)
	node.SetValue(value.MakeType(typ.TupleType{Members: members}))
	return nil
}

func (node *TupleExpr) checkExpr(c *checker) error {
	typeMembers := make([]typ.Type, len(node.Exprs))
	valueMembers := make([]value.Value, len(node.Exprs))
	for i, expr := range node.Exprs {
		if node.IsVars[i] {
			return fmt.Errorf("unexpected 'var'")
		}
		t, v, err := c.checkExpr(expr)
		if err != nil {
			return err
		}
		typeMembers[i] = t
		valueMembers[i] = v
	}
	node.SetType(typ.TupleType{Members: typeMembers})
	node.SetValue(value.Tuple{Members: valueMembers})
	return nil
}

func (node *TupleExpr) Emit(e *emitter) string {
	t, ok := typ.As[typ.TupleType](node.Type())
	if !ok {
		panic("TupleExpr is not TupleType")
	}
	e.DeclareTupleType(t)

	// TODO manage tuple vars and consts
	// TODO manage assignment coercion

	b := strings.Builder{}
	for i, arg := range node.Exprs {
		if i > 0 {
			b.WriteString(", ")
		}
		b.WriteString(arg.Emit(e))
	}
	return fmt.Sprintf("(%s){%s}", node.Type().Emit(), b.String())
}

func (c *converter) VisitTupleExpr(ctx *parser.TupleExprContext) any {
	n := len(ctx.AllTupleElem())
	isVars, exprs := make([]bool, n), make([]Node, n)
	for i, elem := range ctx.AllTupleElem() {
		isVars[i] = elem.VAR() != nil
		exprs[i] = c.Visit(elem.Expr()).(Node)
	}
	return NewTupleExpr(isVars, exprs)
}
