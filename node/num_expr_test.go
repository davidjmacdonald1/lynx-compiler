package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func TestNumExpr(t *testing.T) {
	testExpr(t, "1", typ.UntypedInt, "1")
	testParserErr(t, "1a")
	testCheckerErr(t, "1_")

	testExpr(t, "0.", typ.UntypedFloat, "0")
	testExpr(t, ".0", typ.UntypedFloat, "0")
	testExpr(t, "0.0", typ.UntypedFloat, "0")
	testExpr(t, "3.1415", typ.UntypedFloat, "3.1415")
	testExpr(t, "1_0.0_1", typ.UntypedFloat, "10.01")
	testCheckerErr(t, "1._0")

	testExpr(t, "0b1101", typ.UntypedInt, "13")
	testParserErr(t, "0b2")

	testExpr(t, "0o776", typ.UntypedInt, "510")
	testParserErr(t, "0o8")

	testExpr(t, "0xfFa0", typ.UntypedInt, "65440")
	testParserErr(t, "0xg")
}
