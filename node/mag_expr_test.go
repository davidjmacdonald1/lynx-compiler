package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func TestMagExpr(t *testing.T) {
	testExpr(t, "|-10|", typ.UntypedInt, "10")
	testExpr(t, "||1.2||", typ.UntypedFloat, "1.2")
	testExpr(t, "|true|", typ.UntypedInt, "1")
	testExpr(t, "const x = 10; |x|", typ.UntypedInt, "10")
	testExpr(t, "val b = true; |b|", typ.UntypedInt, "((int64_t)(lynx_b))")
	testExpr(t, "val x Int8 = 10; |x|", typ.UInt8,
		"({ int8_t expr = lynx_x; ((expr > 0)? expr : -expr); })",
	)
	testExpr(t, "val x = 10; |x|", typ.UInt,
		"({ int64_t expr = lynx_x; ((expr > 0)? expr : -expr); })",
	)
	testExpr(t, "val x = 1.5; |x|", typ.Float,
		"({ double expr = lynx_x; ((expr > 0)? expr : -expr); })",
	)

	testParserErr(t, "| |")
}
