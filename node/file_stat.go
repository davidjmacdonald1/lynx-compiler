package node

import (
	"strings"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/scope"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type FileStat struct {
	aNode
	Stats []Node

	scope scope.Scope
}

func NewFileStat(stats []Node) *FileStat {
	return &FileStat{Stats: stats}
}

func (node *FileStat) Check(c *checker) error {
	for _, stat := range node.Stats {
		err := stat.Check(c)
		if err != nil {
			return err
		}
	}
	node.scope = c.Scope
	node.SetType(typ.Unit)
	node.SetValue(value.Unit)
	return nil
}

func (node *FileStat) Emit(e *emitter) string {
	e.Scope = node.scope
	b := strings.Builder{}
	for _, stat := range node.Stats {
		b.WriteString(stat.Emit(e))
	}
	return b.String()
}

func (c *converter) VisitFileStat(ctx *parser.FileStatContext) any {
	stats := make([]Node, len(ctx.AllEndedStat()))
	for i, stat := range ctx.AllEndedStat() {
		stats[i] = c.Visit(stat).(Node)
	}
	return NewFileStat(stats)
}
