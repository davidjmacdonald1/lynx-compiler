package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type DerefExpr struct {
	aNode
	Expr Node
}

func NewDerefExpr(expr Node) *DerefExpr {
	return &DerefExpr{Expr: expr}
}

func (node *DerefExpr) Check(c *checker) error {
	t, _, err := c.checkExpr(node.Expr)
	if err != nil {
		return err
	}

	ptr, ok := typ.As[typ.PointerType](t)
	if !ok {
		return fmt.Errorf("can only dereference pointers")
	}

	node.SetType(ptr.Deref)
	node.SetValue(value.None{})
	return nil
}

func (node *DerefExpr) Emit(e *emitter) string {
	expr := node.Expr.Emit(e)
	return fmt.Sprintf("(*%s)", expr)
}

func (c *converter) VisitDerefExpr(ctx *parser.DerefExprContext) any {
	return NewDerefExpr(c.Visit(ctx.Expr()).(Node))
}
