package node

import (
	"fmt"
	"strings"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/scope"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type MultiStat struct {
	aNode
	Stats []Node

	scope scope.Scope
}

func NewMultiStat(stats []Node) *MultiStat {
	return &MultiStat{Stats: stats}
}

func (node *MultiStat) Check(c *checker) error {
	c.PushScope()
	defer func() {
		node.scope = c.PopScope()
	}()

	if len(node.Stats) == 0 {
		node.SetType(typ.Unit)
		node.SetValue(value.Unit)
		return nil
	}

	for _, stat := range node.Stats {
		err := stat.Check(c)
		if err != nil {
			return err
		}

		node.SetType(stat.Type())
		if node.Value() == nil || node.Value().IsConst() {
			node.SetValue(stat.Value())
		}
	}
	return nil
}

func (node *MultiStat) Emit(e *emitter) string {
	if node.Value().IsConst() {
		return node.Value().String()
	}

	b := strings.Builder{}
	b.WriteString("({\n")
	e.PushScope(node.scope)

	for _, stat := range node.Stats {
		fmt.Fprintf(&b, "%s%s;\n", e.Tab(), stat.Emit(e))
	}

	e.PopScope()
	fmt.Fprintf(&b, "%s})", e.Tab())
	return b.String()
}

func (c *converter) VisitMultiStat(ctx *parser.MultiStatContext) any {
	stats := make([]Node, len(ctx.AllStat()))
	for i, stat := range ctx.AllStat() {
		stats[i] = c.Visit(stat).(Node)
	}
	return NewMultiStat(stats)
}

func (c *converter) VisitMultiStat_(ctx *parser.MultiStat_Context) any {
	return c.Visit(ctx.MultiStat())
}
