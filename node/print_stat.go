package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type PrintStat struct {
	aNode
	Expr Node
}

func NewPrintStat(expr Node) *PrintStat {
	return &PrintStat{Expr: expr}
}

func (node *PrintStat) Check(c *checker) error {
	_, _, err := c.checkExpr(node.Expr)
	node.SetType(typ.Unit)
	node.SetValue(value.None{})
	return err
}

func (node *PrintStat) Emit(e *emitter) string {
	e.Include("stdio")
	expr := node.Expr.Emit(e)
	t := node.Expr.Type()

	var format string
	var cast string
	if typ.Is[typ.IntType](t) {
		tt, _ := typ.As[typ.IntType](t)
		if tt.IsSigned {
			format = `"%" PRId64 "\n"`
			cast = "int64_t"
		} else {
			format = `"%" PRIu64 "\n"`
			cast = "uint64_t"
		}
	} else if typ.Is[typ.FloatType](t) {
		format = `"%lf\n"`
		cast = "double"
	} else if typ.Is[typ.BoolType](t) {
		return fmt.Sprintf(`printf("%%s\n", (%s)? "true" : "false")`, expr)
	} else if typ.Is[typ.PointerType](t) {
		format = `"%p\n"`
		cast = "void *"
	} else {
		panic(fmt.Sprintf("cannot print type %s", t))
	}

	return fmt.Sprintf(`printf(%s, (%s)(%s))`, format, cast, expr)
}

func (c *converter) VisitPrintStat(ctx *parser.PrintStatContext) any {
	return NewPrintStat(c.Visit(ctx.Expr()).(Node))
}
