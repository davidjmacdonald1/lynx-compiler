package node

import (
	"testing"
)

func TestDeclStat(t *testing.T) {
	testStats(t, "val x = 10", "int64_t lynx_x = 10;\n")
	testStats(t, "val x Int = 10", "int64_t lynx_x = 10;\n")
	testStats(t, "val x Int8 = 10", "int8_t lynx_x = 10;\n")
	testStats(t, "val x Float32 = 10", "float lynx_x = 10;\n")
	testCheckerErr(t, "val x Bool = 10")
	testCheckerErr(t, "val x Int8 = true")
	testCheckerErr(t, "val x Type = Int")
	testCheckerErr(t, "var x Type = Type")

	testStats(t, "val y = 1.0", "double lynx_y = 1;\n")
	testStats(t, "val y UInt16 = 1.0", "uint16_t lynx_y = 1;\n")
	testStats(t, "const x = 1.0", "// const x untyped Float = 1;\n")
	testStats(t, "const x = 1.0; val y Int8 = x", "// const x untyped Float = 1;\nint8_t lynx_y = 1;\n")

	testStats(t, "val b = true", "bool lynx_b = true;\n")
	testStats(t, "val b Bool = false", "bool lynx_b = false;\n")

	testCheckerErr(t, "val x = 10; x = 11")
	testStats(t, "var x = 10; x = 11", "int64_t lynx_x = 10;\nlynx_x = 11;\n")

	testStats(t, "val x = 0; val p = &x", "int64_t lynx_x = 0;\nint64_t* lynx_p = &lynx_x;\n")
	testStats(t, "val x = 0; val p ^Int = &x", "int64_t lynx_x = 0;\nint64_t* lynx_p = &lynx_x;\n")
	testStats(t, "var x = 0; val p = &x", "int64_t lynx_x = 0;\nint64_t* lynx_p = &lynx_x;\n")
	testStats(t, "var x = 0; val p ^Int = &x", "int64_t lynx_x = 0;\nint64_t* lynx_p = &lynx_x;\n")
	testStats(t, "var x = 0; val p = &var x", "int64_t lynx_x = 0;\nint64_t* lynx_p = &lynx_x;\n")
	testStats(t, "var x = 0; val p ^Int = &var x", "int64_t lynx_x = 0;\nint64_t* lynx_p = &lynx_x;\n")
	testStats(t, "var x = 0; val p ^var Int = &var x", "int64_t lynx_x = 0;\nint64_t* lynx_p = &lynx_x;\n")
	testCheckerErr(t, "var x = 0; val p ^var Int = &x")
}
