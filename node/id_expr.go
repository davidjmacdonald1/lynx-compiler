package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

type IdExpr struct {
	aNode
	Name string
}

func NewIdExpr(name string) *IdExpr {
	return &IdExpr{Name: name}
}

func (node *IdExpr) Check(c *checker) error {
	tv, ok := c.Scope.Lookup(node.Name)
	if !ok {
		return fmt.Errorf("no such name %s", node.Name)
	}

	node.SetType(tv.Type)
	node.SetValue(tv.Value)
	return nil
}

func (node *IdExpr) Emit(e *emitter) string {
	if node.Value().IsConst() {
		if typ.Is[typ.TupleType](node.Type()) {
			return fmt.Sprintf("(%s){%s}", node.Type().Emit(), node.Value())
		}
		return node.Value().String()
	}
	return e.LynxName(node.Name)
}

func (c *converter) VisitIdExpr(ctx *parser.IdExprContext) any {
	return NewIdExpr(ctx.ID().GetText())
}
