package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func TestPrefixExpr(t *testing.T) {
	testExpr(t, "-5", typ.UntypedInt, "-5")
	testExpr(t, "--5", typ.UntypedInt, "5")
	testExpr(t, "-1.1", typ.UntypedFloat, "-1.1")
	testExpr(t, "+10", typ.UntypedInt, "10")
	testExpr(t, "~1", typ.UntypedInt, "-2")
	testExpr(t, "-~1", typ.UntypedInt, "2")
	testExpr(t, "~-1", typ.UntypedInt, "0")
	testExpr(t, "not true", typ.UntypedBool, "false")
	testExpr(t, "not not false", typ.UntypedBool, "false")

	tt := typ.PointerType{Deref: typ.DeclType{IsVar: false, Type: typ.Int}}
	testExpr(t, "val x = 0; &x", tt, "&lynx_x")

	tt.Deref = typ.DeclType{IsVar: true, Type: typ.Bool}
	testExpr(t, "var b = true; &var b", tt, "&lynx_b")

	testCheckerErr(t, "~false")
	testCheckerErr(t, "~1.5")

	testCheckerErr(t, "not 10")
	testCheckerErr(t, "not 1.5")

	testCheckerErr(t, "-true")
	testCheckerErr(t, "+true")

	testCheckerErr(t, "&10")
	testCheckerErr(t, "&1.5")
	testCheckerErr(t, "&true")

	testCheckerErr(t, "&var false")
	testCheckerErr(t, "&var 1.5")
	testCheckerErr(t, "&var 10")
}
