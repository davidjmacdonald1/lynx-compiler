package node

import (
	"fmt"
	"math/big"
	"strings"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type NumExpr struct {
	aNode
	Text string
}

func NewNumExpr(text string) *NumExpr {
	return &NumExpr{Text: text}
}

func (node *NumExpr) Check(c *checker) error {
	if strings.Contains(node.Text, ".") {
		node.SetType(typ.UntypedFloat)
		val, ok := big.NewFloat(0).SetString(node.Text)
		if !ok {
			return fmt.Errorf("invalid float literal %q", node.Text)
		}
		node.SetValue(value.Float{Val: val})
	} else {
		node.SetType(typ.UntypedInt)
		val, ok := big.NewInt(0).SetString(node.Text, 0)
		if !ok {
			return fmt.Errorf("invalid int literal %q", node.Text)
		}
		node.SetValue(value.Int{Val: val})
	}
	return nil
}

func (node *NumExpr) Emit(e *emitter) string {
	return node.Value().String()
}

func (c *converter) VisitNumExpr(ctx *parser.NumExprContext) any {
	return NewNumExpr(ctx.NUM().GetText())
}
