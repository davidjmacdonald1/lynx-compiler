package node

import (
	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type PointerTypeExpr struct {
	aNode
	IsVar bool
	Expr  Node
}

func NewPointerTypeExpr(isVar bool, expr Node) *PointerTypeExpr {
	return &PointerTypeExpr{IsVar: isVar, Expr: expr}
}

func (node *PointerTypeExpr) Check(c *checker) error {
	tType, err := c.checkType(node.Expr)
	if err != nil {
		return err
	}

	deref := typ.DeclType{IsVar: node.IsVar, Type: tType}
	t := typ.PointerType{Deref: deref}
	node.SetType(typ.Type_)
	node.SetValue(value.MakeType(t))
	return nil
}

func (node *PointerTypeExpr) Emit(e *emitter) string {
	panic("cannot emit PointerTypeExpr")
}

func (c *converter) VisitPointerTypeExpr(
	ctx *parser.PointerTypeExprContext,
) any {
	isVar := ctx.VAR() != nil
	return NewPointerTypeExpr(isVar, c.Visit(ctx.Expr()).(Node))
}
