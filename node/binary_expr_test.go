package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func TestBinaryExpr(t *testing.T) {
	testExpr(t, "1 - 4 * 7 + -3", typ.UntypedInt, "-30")
	testExpr(t, "2^4 / 5 >> 1", typ.UntypedInt, "1")
	testExpr(t, "1 << 4", typ.UntypedInt, "16")
	testExpr(t, "true and false", typ.UntypedBool, "false")
	testExpr(t, "1~2", typ.UntypedInt, "3")
	testExpr(t, "12|5&3", typ.UntypedInt, "13")
	testExpr(t, "val x = 2; x + 1", typ.Int, "lynx_x + 1")
	testExpr(t, "val x = 2; 1 + x", typ.Int, "1 + lynx_x")

	testCheckerErr(t, "0^0")
	testCheckerErr(t, "1 % 0")
	testCheckerErr(t, "1 / 0")

	testCheckerErr(t, "0.0^0")
	testCheckerErr(t, "1.0 % 0")
	testCheckerErr(t, "1.0 / 0")
	testCheckerErr(t, "val x = 0; x % 0")
	testCheckerErr(t, "val x = 0; x / 0")
	testCheckerErr(t, "val x = 0.0; x % 0")
	testCheckerErr(t, "val x = 0.0; x / 0")
	testCheckerErr(t, "1 / (10 - 5 + -5)")

	testCheckerErr(t, "1.0 | 2.0")
	testCheckerErr(t, "1.0 & 2.0")
	testCheckerErr(t, "1.0 ~ 2.0")
	testCheckerErr(t, "1.0 >> 2.0")
	testCheckerErr(t, "1.0 << 2.0")
	testCheckerErr(t, "1 and 2")
	testCheckerErr(t, "1 and false")
	testCheckerErr(t, "1 or 2")
	testCheckerErr(t, "1.0 or 2.0")
}
