package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

type MagExpr struct {
	aNode
	Expr Node
}

func NewMagExpr(expr Node) *MagExpr {
	return &MagExpr{Expr: expr}
}

func (node *MagExpr) Check(c *checker) error {
	t, v, err := c.checkExpr(node.Expr)
	if err != nil {
		return err
	}

	t, err = t.MagnitudeOp()
	if err != nil {
		return err
	}

	node.SetType(t)
	node.SetValue(v.MagnitudeOp())
	return nil
}

func (node *MagExpr) Emit(e *emitter) string {
	if node.Value().IsConst() {
		return node.Value().String()
	}

	expr := node.Expr.Emit(e)
	t := node.Expr.Type()
	if typ.Is[typ.BoolType](t) {
		return fmt.Sprintf("((%s)(%s))", typ.Int.Emit(), expr)
	}

	if !typ.Is[typ.IntType](t) && !typ.Is[typ.FloatType](t) {
		panic(fmt.Sprintf("cannot emit |%s|", t))
	}

	return fmt.Sprintf(
		"({ %s expr = %s; ((expr > 0)? expr : -expr); })", t.Emit(), expr,
	)
}

func (c *converter) VisitMagExpr(ctx *parser.MagExprContext) any {
	return NewMagExpr(c.Visit(ctx.Expr()).(Node))
}
