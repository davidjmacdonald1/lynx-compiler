package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func TestDerefExpr(t *testing.T) {
	testExpr(t, "val x = 0; (&x)^", typ.Int, "(*(&lynx_x))")
	testExpr(t, "val x = 0; val p = &x; p^", typ.Int, "(*lynx_p)")
	testExpr(t, "val x Int8 = 0; val p = &x; p^", typ.Int8, "(*lynx_p)")
	testExpr(t, "var x = 0; val p = &x; (&p^)^", typ.Int, "(*(&(*lynx_p)))")

	tt := typ.PointerType{Deref: typ.DeclType{IsVar: false, Type: typ.Int}}
	testExpr(t, "val x = 0; val p = &x; &p^", tt, "&(*lynx_p)")

	tt.Deref = typ.DeclType{IsVar: true, Type: typ.Float32}
	testExpr(t, "var x = Float32(0); val p = &var x; &var p^", tt, "&(*lynx_p)")

	tt.Deref = typ.DeclType{IsVar: false, Type: typ.Float32}
	testExpr(t, "var x = Float32(0); val p = &var x; &p^", tt, "&(*lynx_p)")
}
