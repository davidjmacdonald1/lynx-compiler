package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func TestParenExpr(t *testing.T) {
	testExpr(t, "(10)", typ.UntypedInt, "10")
	testExpr(t, "(1 + 2) * 3", typ.UntypedInt, "9")
	testExpr(t, "(1 + 2)^2", typ.UntypedInt, "9")
	testParserErr(t, "(1")
}
