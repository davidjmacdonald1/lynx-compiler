package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/scope"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type DeclStat struct {
	aNode
	IsVar bool
	Name  string
	Hint  Node
	Expr  Node

	exprType typ.Type
}

func NewDeclStat(isVar bool, name string, hint, expr Node) *DeclStat {
	return &DeclStat{IsVar: isVar, Name: name, Hint: hint, Expr: expr}
}

func (node *DeclStat) Check(c *checker) error {
	t, v, err := c.checkExpr(node.Expr)
	if err != nil {
		return err
	}

	if node.Hint != nil {
		hint, err := c.checkType(node.Hint)
		if err != nil {
			return err
		}

		if !t.MayCoerceTo(hint) {
			return fmt.Errorf("cannot assign %s to %s", t, hint)
		}
		t = hint
	}

	t = typ.BaseType(t.InferType())
	_, err = v.ConvertTo(t)
	if err != nil {
		return err
	}

	t = typ.DeclType{IsVar: node.IsVar, Type: t}
	v = value.None{}

	node.SetType(typ.Unit)
	node.SetValue(value.Unit)
	node.Expr.SetAssignedTo(t)
	return c.Scope.Define(node.Name, scope.TypedValue{Type: t, Value: v})
}

func (node *DeclStat) Emit(e *emitter) string {
	t := node.Expr.AssignedTo().Emit()
	expr := node.Expr.Emit(e)
	return fmt.Sprintf("%s %s = %s", t, e.LynxName(node.Name), expr)
}

func (c *converter) VisitDeclStat(ctx *parser.DeclStatContext) any {
	var hint Node
	if ctx.Hint != nil {
		hint = c.Visit(ctx.Hint).(Node)
	}
	expr := c.Visit(ctx.Right).(Node)
	isVar := ctx.Key.GetText() == "var"
	return NewDeclStat(isVar, ctx.ID().GetText(), hint, expr)
}
