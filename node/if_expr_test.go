package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func TestIfExpr(t *testing.T) {
	testExpr(t, "if true 1 else 2", typ.UntypedInt, "1")
	testExpr(t, "if true { 1 } else { 2 }", typ.UntypedInt, "1")
	testExpr(t, "val x = true; if x 1 else 2", typ.UntypedInt, "(lynx_x)? 1 : 2")
	testExpr(t, "val x = 4; if true { x + 2 } else { x }", typ.Int, "({\n    lynx_x + 2;\n})")
	testStats(t, "if true { print(10) }", "({\n    printf(\"%\" PRId64 \"\\n\", (int64_t)(10));\n});\n")

	testStats(t, "val x = if true {1} else {2}", "int64_t lynx_x = 1;\n")
	testCheckerErr(t, "if true { true } else { 1 }")
	testCheckerErr(t, "val x = if true {1}")

	testCheckerErr(t, "val y = 1; const x = if y > 1 {1} else {2}")
	testCheckerErr(t, "val y = 1; const x = if true {y} else {2}")
}
