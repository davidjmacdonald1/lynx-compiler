package node

import (
	"fmt"
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/scope"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func testParserErr(t *testing.T, input string) {
	t.Helper()

	_, err := ParseString(input)
	if err == nil {
		t.Fatal(msg(input, "any parser err", "no err", nil))
	}
}

func testCheckerErr(t *testing.T, input string) {
	t.Helper()

	tree, err := ParseString(input)
	if err != nil {
		actual := fmt.Sprintf("parser err: %q", err)
		t.Fatal(msg(input, "any checker err", actual, nil))
	}

	c := newChecker()
	err = tree.Check(c)
	if err == nil {
		t.Fatal(msg(input, "any checker err", "no err", c.Scope))
	}
}

func testExpr(t *testing.T, input string, tt typ.Type, emit string) {
	t.Helper()

	tree, err := ParseString(input)
	if err != nil {
		actual := fmt.Sprintf("parser err: %s", err)
		t.Fatal(msg(input, "valid expr", actual, nil))
	}

	c := newChecker()
	err = tree.Check(c)
	if err != nil {
		actual := fmt.Sprintf("checker err: %s", err)
		t.Fatal(msg(input, "valid expr", actual, c.Scope))
	}

	fileStat := tree.(*FileStat)
	endedStat := fileStat.Stats[len(fileStat.Stats)-1].(*EndedStat)
	exprStat := endedStat.Stat.(*ExprStat)
	expr := exprStat.Expr

	if typ.BaseType(tt) != typ.BaseType(expr.Type()) {
		expected := fmt.Sprintf("type %q", typ.BaseType(tt))
		actual := fmt.Sprintf("type %q", typ.BaseType(expr.Type()))
		t.Fatal(msg(input, expected, actual, c.Scope))
	}

	e := newEmitter()
	e.Depth = 0
	emitted := expr.Emit(e)
	if emit != emitted {
		expected := fmt.Sprintf("emit %q", emit)
		actual := fmt.Sprintf("emit %q", emitted)
		t.Fatal(msg(input, expected, actual, c.Scope))
	}
}

func testStats(t *testing.T, input, emit string) {
	t.Helper()

	tree, err := ParseString(input)
	if err != nil {
		actual := fmt.Sprintf("parser err: %s", err)
		t.Fatal(msg(input, "valid stats", actual, nil))
	}

	c := newChecker()
	err = tree.Check(c)
	if err != nil {
		actual := fmt.Sprintf("checker err: %s", err)
		t.Fatal(msg(input, "valid stats", actual, c.Scope))
	}

	e := newEmitter()
	e.Depth = 0
	emitted := tree.Emit(e)
	if emit != emitted {
		expected := fmt.Sprintf("emit %q", emit)
		actual := fmt.Sprintf("emit %q", emitted)
		t.Fatal(msg(input, expected, actual, c.Scope))
	}
}

func msg(input, expected, actual string, syms scope.Scope) string {
	symText := ""
	if syms != nil {
		symText = fmt.Sprintf("\n\nsymbols:%s", syms)
	}

	return fmt.Sprintf("\ninput   : %q\nexpected: %s\nactual  : %s%s",
		input, expected, actual, symText,
	)
}
