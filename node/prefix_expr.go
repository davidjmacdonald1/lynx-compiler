package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
)

type PrefixExpr struct {
	aNode
	Op   string
	Expr Node
}

func NewPrefixExpr(op string, expr Node) *PrefixExpr {
	return &PrefixExpr{Op: op, Expr: expr}
}

func (node *PrefixExpr) Check(c *checker) error {
	t, v, err := c.checkExpr(node.Expr)
	if err != nil {
		return err
	}

	t, err = t.PrefixOp(node.Op)
	if err != nil {
		return err
	}

	node.SetType(t)
	node.SetValue(v.PrefixOp(node.Op))
	return nil
}

func (node *PrefixExpr) Emit(e *emitter) string {
	if node.Value().IsConst() {
		return node.Value().String()
	}

	var op string
	switch node.Op {
	case "&var":
		op = "&"
	case "~":
		op = "!"
	default:
		op = node.Op
	}

	expr := node.Expr.Emit(e)
	return fmt.Sprintf("%s%s", op, expr)
}

func (c *converter) VisitAddrExpr(ctx *parser.AddrExprContext) any {
	op := "&"
	if ctx.VAR() != nil {
		op = "&var"
	}
	return NewPrefixExpr(op, c.Visit(ctx.Expr()).(Node))
}

func (c *converter) VisitPrefixExpr(ctx *parser.PrefixExprContext) any {
	return NewPrefixExpr(ctx.Op.GetText(), c.Visit(ctx.Expr()).(Node))
}

func (c *converter) VisitNotExpr(ctx *parser.NotExprContext) any {
	return NewPrefixExpr(ctx.NOT().GetText(), c.Visit(ctx.Expr()).(Node))
}
