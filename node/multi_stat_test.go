package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func TestMultiStat(t *testing.T) {
	testStats(t, "{}", "({});\n")
	testStats(t, "{\n}", "({});\n")
	testStats(t, "{;}", "({});\n")
	testStats(t, "{\n;}", "({});\n")

	testExpr(t, "{1.0}", typ.UntypedFloat, "1")
	testExpr(t, "const x = {true}; x", typ.UntypedBool, "true")
	testExpr(t, "const x = {1; 2}; x", typ.UntypedInt, "2")
	testExpr(t, "val x = {print(2); 3}; x", typ.Int, "lynx_x")
	testCheckerErr(t, "val x = 1; const x = {x; 10}")
	testCheckerErr(t, "{const x = 4}; x")
}
