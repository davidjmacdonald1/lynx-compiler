package node

import (
	"github.com/antlr4-go/antlr/v4"
	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

func Parse(input *antlr.InputStream) (Node, error) {
	tree, err := parser.Parse(input)
	if err != nil {
		return nil, err
	}

	c := converter{}
	return c.Visit(tree).(Node), nil
}

func ParseString(input string) (Node, error) {
	return Parse(antlr.NewInputStream(input))
}

func ParseFile(name string) (Node, error) {
	fs, err := antlr.NewFileStream(name)
	if err != nil {
		return nil, err
	}
	return Parse(&fs.InputStream)
}

func CheckTree(tree Node) error {
	c := newChecker()
	return tree.Check(c)
}

func EmitTree(tree Node) (header, code string) {
	e := newEmitter()
	code = tree.Emit(e)
	return e.Header(), code
}

type Node interface {
	Type() typ.Type
	SetType(t typ.Type)
	Value() value.Value
	SetValue(v value.Value)
	AssignedTo() typ.Type
	SetAssignedTo(t typ.Type)

	Check(c *checker) error
	Emit(e *emitter) string
}

type aNode struct {
	t typ.Type
	v value.Value
	a typ.Type
}

func (node aNode) Type() typ.Type {
	return node.t
}

func (node *aNode) SetType(t typ.Type) {
	node.t = t
}

func (node aNode) Value() value.Value {
	return node.v
}

func (node *aNode) SetValue(v value.Value) {
	node.v = v
}

func (node aNode) AssignedTo() typ.Type {
	return node.a
}

func (node *aNode) SetAssignedTo(t typ.Type) {
	node.a = t
}

type converter struct {
	parser.BaseLynxParserVisitor
}

func (c *converter) Visit(tree antlr.ParseTree) any {
	return tree.Accept(c)
}
