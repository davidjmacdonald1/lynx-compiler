package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/scope"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type checker struct {
	Scope     scope.Scope
	TypeCount int
	ExprStats []Node
}

func newChecker() *checker {
	return &checker{Scope: scope.NewModScope()}
}

func (c *checker) PushScope() {
	c.Scope = scope.NewBlockScope(c.Scope)
}

func (c *checker) PopScope() scope.Scope {
	s := c.Scope
	c.Scope = c.Scope.Parent()
	return s
}

func (c *checker) checkExpr(node Node) (typ.Type, value.Value, error) {
	err := node.Check(c)
	if err != nil {
		return nil, nil, err
	}

	t := node.Type()
	if t == typ.Type_ {
		return nil, nil, fmt.Errorf("expected expression")
	}
	return t, node.Value(), nil
}

func (c *checker) checkType(node Node) (typ.Type, error) {
	c.TypeCount++
	err := node.Check(c)
	c.TypeCount--
	if err != nil {
		return nil, err
	}

	if node.Type() != typ.Type_ {
		return nil, fmt.Errorf("expected type")
	}
	return node.Value().(value.Type).Type, err
}
