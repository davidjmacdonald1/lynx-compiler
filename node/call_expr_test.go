package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func TestCallExpr(t *testing.T) {
	testExpr(t, "Int(1.0)", typ.Int, "1")
	testExpr(t, "val x = 0.0; Int(x)", typ.Int, "((int64_t)(lynx_x))")
	testExpr(t, "val x = 0.0; Float(Bool(x))", typ.Float,
		"((double)(((bool)(lynx_x))))",
	)
	testExpr(t, "const x = 5.0; Int(x)", typ.Int, "5")
	testExpr(t, "Int8(true)", typ.Int8, "1")

	testCheckerErr(t, "Int()")
	testCheckerErr(t, "Int(1, 2)")
}
