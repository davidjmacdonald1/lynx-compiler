package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
)

type EndedStat struct {
	aNode
	Stat Node
}

func NewEndedStat(stat Node) *EndedStat {
	return &EndedStat{Stat: stat}
}

func (node *EndedStat) Check(c *checker) error {
	err := node.Stat.Check(c)
	node.SetType(node.Stat.Type())
	node.SetValue(node.Stat.Value())
	return err
}

func (node *EndedStat) Emit(e *emitter) string {
	return fmt.Sprintf("%s%s;\n", e.Tab(), node.Stat.Emit(e))
}

func (c *converter) VisitEndedStat(ctx *parser.EndedStatContext) any {
	return NewEndedStat(c.Visit(ctx.Stat()).(Node))
}
