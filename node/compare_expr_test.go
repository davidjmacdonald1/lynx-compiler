package node

import (
	"testing"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func testCompareExpr(t *testing.T) {
	testExpr(t, "1 != 2", typ.UntypedBool, "true")
	testExpr(t, "1 < 2 < 3 == 3", typ.UntypedBool, "true")
	testExpr(t, "val x = 0; 1 < x < 3", typ.UntypedBool,
		"(1 < lynx_x) && (lynx_x < 3)",
	)
	testExpr(t, "val x = 2.0; 1.0 < x < 3.2 < 4", typ.UntypedBool,
		"(1 < lynx_x) && (lynx_x < 3.2)",
	)
	testExpr(t, "val x = 0; 1 < 0 < x", typ.UntypedBool, "false")
	testExpr(t, "1 > 0 <= 5 >= 5", typ.UntypedBool, "true")
	testExpr(t, "true == false", typ.UntypedBool, "true")
	testExpr(t, "(1 != 2) == true", typ.UntypedBool, "true")

	testCheckerErr(t, "true < false")
	testCheckerErr(t, "false < 1")
}
