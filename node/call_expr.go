package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type CallExpr struct {
	aNode
	Left Node
	Args []Node
}

func NewCallExpr(left Node, args []Node) *CallExpr {
	return &CallExpr{Left: left, Args: args}
}

func (node *CallExpr) Check(c *checker) error {
	err := node.Left.Check(c)
	if err != nil {
		return err
	}

	if node.Left.Type() != typ.Type_ {
		return fmt.Errorf("cannot call %s", node.Left.Type())
	}
	tt := node.Left.Value().(value.Type).Type

	if len(node.Args) != 1 {
		return fmt.Errorf("expected one expression")
	}

	t, v, err := c.checkExpr(node.Args[0])
	if err != nil {
		return err
	}

	if !t.MayCastTo(tt) {
		return fmt.Errorf("cannot cast %s to %s", t, tt)
	}

	v, err = v.ConvertTo(tt)
	if err != nil {
		return err
	}

	node.SetType(tt)
	node.SetValue(v)
	return nil
}

func (node *CallExpr) Emit(e *emitter) string {
	if node.Value().IsConst() {
		return node.Value().String()
	}
	expr := node.Args[0].Emit(e)
	return fmt.Sprintf("((%s)(%s))", node.Type().Emit(), expr)
}

func (c *converter) VisitCallExpr(ctx *parser.CallExprContext) any {
	left := c.Visit(ctx.Left).(Node)
	args := make([]Node, len(ctx.AllExpr())-1)
	for i, expr := range ctx.AllExpr()[1:] {
		args[i] = c.Visit(expr).(Node)
	}
	return NewCallExpr(left, args)
}
