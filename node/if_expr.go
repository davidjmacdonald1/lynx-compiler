package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type IfExpr struct {
	aNode
	Cond Node
	Then Node
	Else Node
}

func NewIfExpr(cond, then, else_ Node) *IfExpr {
	return &IfExpr{Cond: cond, Then: then, Else: else_}
}

func (node *IfExpr) Check(c *checker) error {
	ct, cv, err := c.checkExpr(node.Cond)
	if err != nil {
		return err
	}
	if !typ.Is[typ.BoolType](ct) {
		return fmt.Errorf("expecting Bool")
	}

	tt, tv, err := c.checkExpr(node.Then)
	if err != nil {
		return err
	}

	et, ev := typ.Type(typ.Unit), value.Value(value.Unit)
	if node.Else != nil {
		et, ev, err = c.checkExpr(node.Else)
		if err != nil {
			return err
		}
	} else {
		for _, stat := range c.ExprStats {
			if node == stat {
				node.SetType(typ.Unit)
				node.SetValue(value.None{})
				return nil
			}
		}
		return fmt.Errorf("if expression must have an else clause")
	}

	t := typ.CommonType(tt, et)
	if t == nil {
		return fmt.Errorf("if expression type mismatch (%s and %s)", tt, et)
	}
	node.SetType(t)

	if !cv.IsConst() {
		node.SetValue(value.None{})
		return nil
	}

	if cv.(value.Bool).Val {
		node.SetValue(tv)
	} else {
		node.SetValue(ev)
	}
	return nil
}

func (node *IfExpr) Emit(e *emitter) string {
	if node.Value().IsConst() {
		return node.Value().String()
	}

	if node.Cond.Value().IsConst() {
		if node.Cond.Value().(value.Bool).Val {
			return node.Then.Emit(e)
		} else if node.Else != nil {
			return node.Else.Emit(e)
		} else {
			return "({})"
		}
	}

	cond := node.Cond.Emit(e)
	then := node.Then.Emit(e)
	else_ := "({})"
	if node.Else != nil {
		else_ = node.Else.Emit(e)
	}
	return fmt.Sprintf("(%s)? %s : %s", cond, then, else_)
}

func (c *converter) VisitIfExpr(ctx *parser.IfExprContext) any {
	cond := c.Visit(ctx.Cond).(Node)
	then := c.Visit(ctx.Then).(Node)

	var else_ Node
	if ctx.Else != nil {
		else_ = c.Visit(ctx.Else).(Node)
	}

	return NewIfExpr(cond, then, else_)
}
