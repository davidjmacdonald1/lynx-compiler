package node

import (
	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
)

type ExprStat struct {
	aNode
	Expr Node
}

func NewExprStat(expr Node) *ExprStat {
	return &ExprStat{Expr: expr}
}

func (node *ExprStat) Check(c *checker) error {
	c.ExprStats = append(c.ExprStats, node.Expr)
	t, v, err := c.checkExpr(node.Expr)
	c.ExprStats = c.ExprStats[:len(c.ExprStats)-1]

	node.SetType(t)
	node.SetValue(v)
	return err
}

func (node *ExprStat) Emit(e *emitter) string {
	return node.Expr.Emit(e)
}

func (c *converter) VisitExprStat(ctx *parser.ExprStatContext) any {
	return NewExprStat(c.Visit(ctx.Expr()).(Node))
}
