package node

import (
	"testing"
)

func TestConstStat(t *testing.T) {
	testStats(t, "const x = 14; x + 2", "// const x untyped Int = 14;\n16;\n")
	testStats(t, "const x Int8 = 1", "// const x Int8 = 1;\n")
	testCheckerErr(t, "const x Int8 = true")
	testCheckerErr(t, "const x Int8 = 1.2")
	testStats(t, "const x = 5; val y = x", "// const x untyped Int = 5;\nint64_t lynx_y = 5;\n")
	testCheckerErr(t, "val x = 5; const y = x")

	testStats(t, "const NewInt = Int; val x NewInt = 1", "// const NewInt Type = Int64;\nint64_t lynx_x = 1;\n")
	testStats(t, "const MyInt = Int32; val x MyInt = MyInt(3)", "// const MyInt Type = Int32;\nint32_t lynx_x = 3;\n")
	testStats(t, "const I32 = Int32; val x = 4; I32(x)", "// const I32 Type = Int32;\nint64_t lynx_x = 4;\n((int32_t)(lynx_x));\n")
	testStats(t, "const I64 Type = Int64", "// const I64 Type = Int64;\n")
	testCheckerErr(t, "const I = Type(Int)")
	testStats(t, "const Tip Type = Type", "// const Tip Type = Type;\n")
}
