package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
)

type ParenExpr struct {
	aNode
	Expr Node
}

func NewParenExpr(expr Node) *ParenExpr {
	return &ParenExpr{Expr: expr}
}

func (node *ParenExpr) Check(c *checker) error {
	t, v, err := c.checkExpr(node.Expr)
	node.SetType(t)
	node.SetValue(v)
	return err
}

func (node *ParenExpr) Emit(e *emitter) string {
	if node.Value().IsConst() {
		return node.Value().String()
	}
	expr := node.Expr.Emit(e)
	return fmt.Sprintf("(%s)", expr)
}

func (c *converter) VisitParenExpr(ctx *parser.ParenExprContext) any {
	return NewParenExpr(c.Visit(ctx.Expr()).(Node))
}
