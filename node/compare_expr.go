package node

import (
	"fmt"
	"strings"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type CompareExpr struct {
	aNode
	Ops   []string
	Exprs []Node

	compares []value.Value
}

func NewCompareExpr(ops []string, exprs []Node) *CompareExpr {
	return &CompareExpr{Ops: ops, Exprs: exprs}
}

func (node *CompareExpr) Check(c *checker) error {
	leftT, leftV, err := c.checkExpr(node.Exprs[0])
	if err != nil {
		return err
	}

	node.compares = make([]value.Value, len(node.Exprs)-1)
	var t typ.Type = typ.UntypedBool
	var v value.Value = value.Bool{Val: true}
	for i, expr := range node.Exprs[1:] {
		rightT, rightV, err := c.checkExpr(expr)
		if err != nil {
			return err
		}

		op := node.Ops[i]
		as, compareT, err := typ.BinaryOp(op, leftT, rightT)
		if err != nil {
			return err
		}

		compare, err := value.BinaryOpAs(as, op, leftV, rightV)
		if err != nil {
			return err
		}

		node.compares[i] = compare
		as, t, err = typ.BinaryOp("and", t, compareT)
		if err != nil {
			return err
		}

		v, err = value.BinaryOpAs(as, "and", v, compare)
		if err != nil {
			return err
		}
		leftT, leftV = rightT, rightV
	}

	node.SetType(t)
	node.SetValue(v)
	return nil
}

func (node *CompareExpr) Emit(e *emitter) string {
	b := strings.Builder{}
	for i, compare := range node.compares {
		if compare.IsConst() {
			if compare.String() == "false" {
				return "false"
			}
			continue
		}

		if b.Len() > 0 {
			b.WriteString(" && ")
		}
		left, right := node.Exprs[i].Emit(e), node.Exprs[i+1].Emit(e)
		fmt.Fprintf(&b, "(%s) %s (%s)", left, node.Ops[i], right)
	}

	if b.Len() == 0 {
		return "true"
	}
	return b.String()
}

func (c *converter) VisitCompExpr(ctx *parser.CompExprContext) any {
	return NewCompareExpr(c.getCompExprParts(ctx))
}

func (c *converter) getCompExprParts(
	ctx *parser.CompExprContext,
) ([]string, []Node) {
	if left, ok := ctx.Left.(*parser.CompExprContext); ok {
		ops, exprs := c.getCompExprParts(left)
		right := c.Visit(ctx.Right).(Node)
		return append(ops, ctx.Op.GetText()), append(exprs, right)
	}
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return []string{ctx.Op.GetText()}, []Node{left, right}
}
