package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/scope"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type ConstStat struct {
	aNode
	Name string
	Hint Node
	Expr Node

	exprType typ.Type
}

func NewConstStat(name string, hint, expr Node) *ConstStat {
	return &ConstStat{Name: name, Hint: hint, Expr: expr}
}

func (node *ConstStat) Check(c *checker) error {
	err := node.Expr.Check(c)
	if err != nil {
		return err
	}
	t, v := node.Expr.Type(), node.Expr.Value()

	if node.Hint != nil {
		hint, err := c.checkType(node.Hint)
		if err != nil {
			return err
		}

		if !t.MayCoerceTo(hint) {
			return fmt.Errorf("cannot assign %s to %s", t, hint)
		}
		t = hint
	}

	if t != typ.Type_ {
		if !v.IsConst() {
			return fmt.Errorf("const must have constant value")
		}

		t = typ.BaseType(t)
		v, err = v.ConvertTo(t)
		if err != nil {
			return err
		}
	}

	node.exprType = t
	node.SetType(typ.Unit)
	node.SetValue(value.Unit)
	return c.Scope.Define(node.Name, scope.TypedValue{Type: t, Value: v})
}

func (node *ConstStat) Emit(e *emitter) string {
	t, v := node.exprType, node.Expr.Value()
	return fmt.Sprintf("// const %s %s = %s", node.Name, t, v)
}

func (c *converter) VisitConstStat(ctx *parser.ConstStatContext) any {
	var hint Node
	if ctx.Hint != nil {
		hint = c.Visit(ctx.Hint).(Node)
	}
	expr := c.Visit(ctx.Right).(Node)
	return NewConstStat(ctx.ID().GetText(), hint, expr)
}
