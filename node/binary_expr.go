package node

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/parser"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type BinaryExpr struct {
	aNode
	Op    string
	Left  Node
	Right Node
}

func NewBinaryExpr(op string, left, right Node) *BinaryExpr {
	return &BinaryExpr{Op: op, Left: left, Right: right}
}

func (node *BinaryExpr) Check(c *checker) error {
	tLeft, vLeft, err := c.checkExpr(node.Left)
	if err != nil {
		return err
	}

	tRight, vRight, err := c.checkExpr(node.Right)
	if err != nil {
		return err
	}

	as, t, err := typ.BinaryOp(node.Op, tLeft, tRight)
	if err != nil {
		return err
	}

	v, err := value.BinaryOpAs(as, node.Op, vLeft, vRight)
	if err != nil {
		return err
	}

	node.SetType(t)
	node.SetValue(v)
	if node.Op == "=" {
		node.Right.SetAssignedTo(tLeft)
	}
	return nil
}

func (node *BinaryExpr) Emit(e *emitter) string {
	if node.Value().IsConst() {
		return node.Value().String()
	}

	left, right := node.Left.Emit(e), node.Right.Emit(e)
	switch node.Op {
	case "^":
		e.Include("math")
		return fmt.Sprintf("(%s)pow(%s, %s)", node.Type().Emit(), left, right)
	case "%":
		if typ.Is[typ.FloatType](node.Type()) {
			e.Include("math")
			return fmt.Sprintf("fmod(%s, %s)", left, right)
		}
	}

	var op string
	switch node.Op {
	case "~":
		op = "^"
	case "and":
		op = "&&"
	case "or":
		op = "||"
	default:
		op = node.Op
	}
	return fmt.Sprintf("%s %s %s", left, op, right)
}

func (c *converter) VisitPowExpr(ctx *parser.PowExprContext) any {
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return NewBinaryExpr(ctx.CARET().GetText(), left, right)
}

func (c *converter) VisitMulExpr(ctx *parser.MulExprContext) any {
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return NewBinaryExpr(ctx.Op.GetText(), left, right)
}

func (c *converter) VisitAddExpr(ctx *parser.AddExprContext) any {
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return NewBinaryExpr(ctx.Op.GetText(), left, right)
}

func (c *converter) VisitShiftExpr(ctx *parser.ShiftExprContext) any {
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return NewBinaryExpr(ctx.Op.GetText(), left, right)
}

func (c *converter) VisitBitAndExpr(ctx *parser.BitAndExprContext) any {
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return NewBinaryExpr(ctx.BITAND().GetText(), left, right)
}

func (c *converter) VisitBitXorExpr(ctx *parser.BitXorExprContext) any {
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return NewBinaryExpr(ctx.BITXOR().GetText(), left, right)
}

func (c *converter) VisitBitOrExpr(ctx *parser.BitOrExprContext) any {
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return NewBinaryExpr(ctx.BITOR().GetText(), left, right)
}

func (c *converter) VisitAndExpr(ctx *parser.AndExprContext) any {
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return NewBinaryExpr(ctx.AND().GetText(), left, right)
}

func (c *converter) VisitOrExpr(ctx *parser.OrExprContext) any {
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return NewBinaryExpr(ctx.OR().GetText(), left, right)
}

func (c *converter) VisitReassignExpr(ctx *parser.ReassignExprContext) any {
	left, right := c.Visit(ctx.Left).(Node), c.Visit(ctx.Right).(Node)
	return NewBinaryExpr(ctx.Op.GetText(), left, right)
}
