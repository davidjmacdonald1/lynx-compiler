package typ

import "fmt"

type DeclType struct {
	IsVar bool
	Type  Type
}

func (t DeclType) MayCoerceTo(to Type) bool {
	switch to.(type) {
	case DeclType:
		tt := to.(DeclType)
		return (t.IsVar || !tt.IsVar) && t.Type.MayCoerceTo(tt.Type)
	}
	return false
}

func (t DeclType) MayCastTo(to Type) bool {
	return t.Type.MayCastTo(to)
}

func (t DeclType) MagnitudeOp() (Type, error) {
	return t.Type.MagnitudeOp()
}

func (t DeclType) PrefixOp(op string) (Type, error) {
	switch op {
	case "&var":
		if t.IsVar {
			return PointerType{t}, nil
		}
		return nil, fmt.Errorf("cannot take var address of non-var")
	case "&":
		t := DeclType{IsVar: false, Type: t.Type}
		return PointerType{t}, nil
	}
	return t.Type.PrefixOp(op)
}

func (t DeclType) BinaryOp(op string) (Type, error) {
	switch op {
	case "=":
		if t.IsVar {
			return t, nil
		} else {
			return nil, undefinedBinaryOpErr(op, t)
		}
	}
	return t.Type.BinaryOp(op)
}

func (t DeclType) InferType() Type {
	return DeclType{t.IsVar, t.Type.InferType()}
}

func (t DeclType) String() string {
	if t.IsVar {
		return fmt.Sprintf("var %s", t.Type)
	}
	return fmt.Sprintf("val %s", t.Type)
}

func (t DeclType) Mangle() string {
	return t.Type.Mangle()
}

func (t DeclType) Emit() string {
	return t.Type.Emit()
}
