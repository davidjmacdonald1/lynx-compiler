package typ

import "fmt"

type PointerType struct {
	Deref Type
}

func (t PointerType) MayCoerceTo(to Type) bool {
	to = BaseType(to)
	switch to.(type) {
	case PointerType:
		tt := to.(PointerType)
		if t.Deref.MayCoerceTo(tt.Deref) {
			return true
		}
	}
	return false
}

func (t PointerType) MayCastTo(to Type) bool {
	return t == BaseType(to)
}

func (t PointerType) MagnitudeOp() (Type, error) {
	return nil, undefinedMagnitudeOpErr(t)
}

func (t PointerType) PrefixOp(op string) (Type, error) {
	return nil, undefinedPrefixOpErr(op, t)
}

func (t PointerType) BinaryOp(op string) (Type, error) {
	switch op {
	case "==", "!=":
		return UntypedBool, nil
	}
	return nil, undefinedBinaryOpErr(op, t)
}

func (t PointerType) InferType() Type {
	return PointerType{Deref: t.Deref.InferType()}
}

func (t PointerType) String() string {
	return fmt.Sprintf("^%s", t.Deref)
}

func (t PointerType) Mangle() string {
	return fmt.Sprintf("P_%s_", t.Deref.Mangle())
}

func (t PointerType) Emit() string {
	return fmt.Sprintf("%s*", t.Deref.Emit())
}
