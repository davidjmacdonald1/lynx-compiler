package typ

type Type interface {
	MayCoerceTo(to Type) bool
	MayCastTo(to Type) bool
	MagnitudeOp() (Type, error)
	PrefixOp(op string) (Type, error)
	BinaryOp(op string) (Type, error)
	InferType() Type
	String() string
	Mangle() string
	Emit() string
}

func BinaryOp(op string, left, right Type) (as, t Type, err error) {
	if BaseType(right).MayCoerceTo(left) {
		as = left
		t, err = left.BinaryOp(op)
		return as, t, err
	}
	if BaseType(left).MayCoerceTo(right) {
		as = right
		t, err = right.BinaryOp(op)
		return as, t, err
	}
	return nil, nil, mismatchedTypesErr(left, right)
}

func CommonType(left, right Type) Type {
	if BaseType(right).MayCoerceTo(left) {
		return left
	}
	if BaseType(left).MayCoerceTo(right) {
		return right
	}
	return nil
}

func Is[T Type](t Type) bool {
	_, ok := As[T](t)
	return ok
}

func As[T Type](t Type) (T, bool) {
	tt, ok := BaseType(t).(T)
	return tt, ok
}

func BaseType(t Type) Type {
	switch t.(type) {
	case DeclType:
		return BaseType(t.(DeclType).Type)
	}
	return t
}
