package typ

var UntypedBool = BoolType{false}
var Bool = BoolType{true}

type BoolType struct {
	Typed bool
}

func (t BoolType) MayCoerceTo(to Type) bool {
	to = BaseType(to)
	switch to.(type) {
	case BoolType:
		return t == UntypedBool || t == to
	}
	return false
}

func (t BoolType) MayCastTo(to Type) bool {
	to = BaseType(to)
	switch to.(type) {
	case BoolType, IntType, FloatType:
		return true
	}
	return false
}

func (t BoolType) MagnitudeOp() (Type, error) {
	return UntypedInt, nil
}

func (t BoolType) PrefixOp(op string) (Type, error) {
	switch op {
	case "not":
		return t, nil
	}
	return nil, undefinedPrefixOpErr(op, t)
}

func (t BoolType) BinaryOp(op string) (Type, error) {
	switch op {
	case "and", "or":
		return t, nil
	case "==", "!=":
		return UntypedBool, nil
	}
	return nil, undefinedBinaryOpErr(op, t)
}

func (t BoolType) InferType() Type {
	return Bool
}

func (t BoolType) String() string {
	if t == UntypedBool {
		return "untyped Bool"
	}
	return "Bool"
}

func (t BoolType) Mangle() string {
	return "B"
}

func (t BoolType) Emit() string {
	return "bool"
}
