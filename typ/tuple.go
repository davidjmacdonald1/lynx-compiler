package typ

import (
	"strings"
)

var Unit = TupleType{}

type TupleType struct {
	Members []Type
}

func (t TupleType) MayCoerceTo(to Type) bool {
	to = BaseType(to)
	switch to.(type) {
	case TupleType:
		tt := to.(TupleType)
		if len(t.Members) != len(tt.Members) {
			return false
		}
		for i := range t.Members {
			if !t.Members[i].MayCoerceTo(tt.Members[i]) {
				return false
			}
		}
		return true
	}
	return false
}

func (t TupleType) MayCastTo(to Type) bool {
	return false
}

func (t TupleType) MagnitudeOp() (Type, error) {
	return UInt, nil
}

func (t TupleType) PrefixOp(op string) (Type, error) {
	return nil, undefinedPrefixOpErr(op, t)
}

func (t TupleType) BinaryOp(op string) (Type, error) {
	switch op {
	case "=":
		panic("implement (x, y) = tuple")
	case "==", "!=":
		return UntypedBool, nil
	}
	return nil, undefinedBinaryOpErr(op, t)
}

func (t TupleType) InferType() Type {
	members := make([]Type, len(t.Members))
	for i, member := range t.Members {
		members[i] = DeclType{IsVar: false, Type: member.InferType()}
	}
	return TupleType{members}
}

func (t TupleType) String() string {
	b := strings.Builder{}
	b.WriteRune('(')
	for i, tt := range t.Members {
		if i > 0 {
			b.WriteString(", ")
		}
		b.WriteString(tt.String())
	}
	if len(t.Members) == 1 {
		b.WriteRune(',')
	}
	b.WriteRune(')')
	return b.String()
}

func (t TupleType) Mangle() string {
	b := strings.Builder{}
	b.WriteString("T_")
	for _, member := range t.Members {
		b.WriteString(member.Mangle())
	}
	b.WriteRune('_')
	return b.String()
}

func (t TupleType) Emit() string {
	return t.Mangle()
}
