package typ

import (
	"fmt"
)

func undefinedMagnitudeOpErr(t Type) error {
	return fmt.Errorf("|x| for x of type %s is undefined", t)
}

func undefinedPrefixOpErr(op string, t Type) error {
	if op == "&" || op == "&var" {
		return fmt.Errorf("can only take the address of a binding")
	}
	return fmt.Errorf("%sx for x of type %s is undefined", op, t)
}

func emitErr(t Type) error {
	return fmt.Errorf("cannot emit type %s", t)
}

func undefinedBinaryOpErr(op string, t Type) error {
	if op == "=" {
		return fmt.Errorf("can only reassign a var")
	}
	return fmt.Errorf("x %s y for x, y of type %s is undefined", op, t)
}

func mismatchedTypesErr(left, right Type) error {
	return fmt.Errorf("mismatched types (%s and %s)", left, right)
}
