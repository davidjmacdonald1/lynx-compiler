package typ

import "fmt"

var Type_ = TypeType{}

type TypeType struct{}

func (t TypeType) MayCoerceTo(to Type) bool {
	return to == Type_
}

func (t TypeType) MayCastTo(to Type) bool {
	return to == Type_
}

func (t TypeType) MagnitudeOp() (Type, error) {
	return nil, undefinedMagnitudeOpErr(t)
}

func (t TypeType) PrefixOp(op string) (Type, error) {
	return nil, undefinedPrefixOpErr(op, t)
}

func (t TypeType) BinaryOp(op string) (Type, error) {
	return nil, undefinedBinaryOpErr(op, t)
}

func (t TypeType) InferType() Type {
	panic(fmt.Errorf("must call inferType on type value instead"))
}

func (t TypeType) String() string {
	return fmt.Sprintf("Type")
}

func (t TypeType) Mangle() string {
	panic(fmt.Errorf("must call mangle on type value instead"))
}

func (t TypeType) Emit() string {
	panic(fmt.Errorf("must call emit on type value instead"))
}
