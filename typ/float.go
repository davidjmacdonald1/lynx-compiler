package typ

import "fmt"

var Float = Float64

var UntypedFloat = FloatType{0}
var Float32 = FloatType{32}
var Float64 = FloatType{64}

type FloatType struct {
	Size int
}

func (t FloatType) MayCoerceTo(to Type) bool {
	to = BaseType(to)
	switch to.(type) {
	case FloatType:
		tt := to.(FloatType)
		if t.Size <= tt.Size {
			return true
		}
	case IntType:
		if t == UntypedFloat && to != UntypedInt {
			return true
		}
	}
	return false
}

func (t FloatType) MayCastTo(to Type) bool {
	to = BaseType(to)
	switch to.(type) {
	case BoolType, IntType, FloatType:
		return true
	}
	return false
}

func (t FloatType) MagnitudeOp() (Type, error) {
	return t, nil
}

func (t FloatType) PrefixOp(op string) (Type, error) {
	switch op {
	case "+", "-":
		return t, nil
	}
	return nil, undefinedPrefixOpErr(op, t)
}

func (t FloatType) BinaryOp(op string) (Type, error) {
	switch op {
	case "^", "*", "/", "%", "+", "-":
		return t, nil
	case "<", "<=", ">", ">=", "==", "!=":
		return UntypedBool, nil
	}
	return nil, undefinedBinaryOpErr(op, t)
}

func (t FloatType) InferType() Type {
	if t == UntypedFloat {
		return Float
	}
	return t
}

func (t FloatType) String() string {
	if t == UntypedFloat {
		return "untyped Float"
	}
	return fmt.Sprintf("Float%d", t.Size)
}

func (t FloatType) Mangle() string {
	switch t {
	case UntypedFloat:
		return Float.Mangle()
	}
	return fmt.Sprintf("F%d", t.Size)
}

func (t FloatType) Emit() string {
	switch t {
	case UntypedFloat:
		return Float.Emit()
	case Float32:
		return "float"
	case Float64:
		return "double"
	}
	panic(emitErr(t))
}
