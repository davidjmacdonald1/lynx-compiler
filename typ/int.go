package typ

import "fmt"

var Byte = UInt8
var Int = Int64
var UInt = UInt64

var UntypedInt = IntType{true, 0}
var Int8 = IntType{true, 8}
var Int16 = IntType{true, 16}
var Int32 = IntType{true, 32}
var Int64 = IntType{true, 64}
var UInt8 = IntType{false, 8}
var UInt16 = IntType{false, 16}
var UInt32 = IntType{false, 32}
var UInt64 = IntType{false, 64}

type IntType struct {
	IsSigned bool
	Size     int
}

func (t IntType) MayCoerceTo(to Type) bool {
	to = BaseType(to)
	switch to.(type) {
	case IntType:
		tt := to.(IntType)
		if t == UntypedInt || (t.IsSigned == tt.IsSigned && t.Size <= tt.Size) {
			return true
		}
	case FloatType:
		if t == UntypedInt {
			return true
		}
	}
	return false
}

func (t IntType) MayCastTo(to Type) bool {
	to = BaseType(to)
	switch to.(type) {
	case BoolType, IntType, FloatType:
		return true
	}
	return false
}

func (t IntType) MagnitudeOp() (Type, error) {
	if t == UntypedInt {
		return t, nil
	}
	return IntType{false, t.Size}, nil
}

func (t IntType) PrefixOp(op string) (Type, error) {
	switch op {
	case "+", "-", "~":
		return t, nil
	}
	return nil, undefinedPrefixOpErr(op, t)
}

func (t IntType) BinaryOp(op string) (Type, error) {
	switch op {
	case "^", "*", "/", "%", "+", "-", "<<", ">>", "&", "~", "|":
		return t, nil
	case "<", "<=", ">", ">=", "==", "!=":
		return UntypedBool, nil
	}
	return nil, undefinedBinaryOpErr(op, t)
}

func (t IntType) InferType() Type {
	if t == UntypedInt {
		return Int
	}
	return t
}

func (t IntType) String() string {
	if t == UntypedInt {
		return "untyped Int"
	}
	if t.IsSigned {
		return fmt.Sprintf("Int%d", t.Size)
	}
	return fmt.Sprintf("UInt%d", t.Size)
}

func (t IntType) Mangle() string {
	switch t {
	case UntypedInt:
		return Int.Mangle()
	}

	if t.IsSigned {
		return fmt.Sprintf("I%d", t.Size)
	}
	return fmt.Sprintf("U%d", t.Size)
}

func (t IntType) Emit() string {
	switch t {
	case UntypedInt:
		return Int.Emit()
	}

	if t.IsSigned {
		return fmt.Sprintf("int%d_t", t.Size)
	}
	return fmt.Sprintf("uint%d_t", t.Size)
}
