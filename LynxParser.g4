parser grammar LynxParser;
options { tokenVocab = LynxLexer; }

fileStat : endedStat+
         | EOF
         ;

endedStat : NL? stat lineEnd ;

lineEnd : (NL | SEMICOLON)+
        | EOF
        ;

multiStat : LBRACE NL? (stat lineEnd)* stat? lineEnd? RBRACE ;

stat : CONST ID Hint=expr? ASSIGN Right=expr #constStat
     | Key=(VAL | VAR) ID Hint=expr? ASSIGN Right=expr #declStat
     | PRINT LPAREN NL? expr NL? RPAREN #printStat
	 | expr #exprStat
     ;

tupleElem : VAR? expr ;

expr : NUM #numExpr
     | ID #idExpr
     | LPAREN NL? expr NL? RPAREN #parenExpr
     | BITOR NL? expr NL? BITOR #magExpr
	 | LPAREN NL? (tupleElem COMMA | tupleElem (COMMA NL? tupleElem)+)? NL? RPAREN #tupleExpr
     | Op=CARET VAR? expr #pointerTypeExpr
     | Left=expr LPAREN NL? (expr (COMMA NL? expr)* NL?)? RPAREN #callExpr
     | expr CARET #derefExpr
     | <assoc=right> Left=expr CARET NL? Right=expr #powExpr
	 | BITAND VAR? expr #addrExpr
     | Op=(PLUS | MINUS | BITXOR) expr #prefixExpr
     | Left=expr Op=(STAR | SLASH | PERCENT) NL? Right=expr #mulExpr
     | Left=expr Op=(PLUS | MINUS) NL? Right=expr #addExpr
     | Left=expr Op=(LSHIFT | RSHIFT) NL? Right=expr #shiftExpr
     | Left=expr BITAND NL? Right=expr #bitAndExpr
     | Left=expr BITXOR NL? Right=expr #bitXorExpr
     | Left=expr BITOR NL? Right=expr #bitOrExpr
     | Left=expr Op=(LT | GT | LTE | GTE | EQ | NE) NL? Right=expr #compExpr
     | NOT NL? expr #notExpr
     | Left=expr AND NL? Right=expr #andExpr
     | Left=expr OR NL? Right=expr #orExpr
	 | Left=expr Op=ASSIGN NL? Right=expr #reassignExpr
	 | IF Cond=expr NL? Then=stat (ELSE Else=stat)? #ifExpr
	 | multiStat #multiStat_
     ;
