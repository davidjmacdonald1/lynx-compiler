package scope

import (
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type TypedValue struct {
	Type  typ.Type
	Value value.Value
}
