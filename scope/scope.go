package scope

import (
	"fmt"
	"strings"
)

type Scope interface {
	Parent() Scope
	Define(name string, v TypedValue) error
	Lookup(name string) (TypedValue, bool)
	String() string
}

type BlockScope struct {
	parent Scope
	defs   map[string]TypedValue
}

func NewModScope() Scope {
	if Builtins == nil {
		initBuiltins()
	}
	return &BlockScope{Builtins, make(map[string]TypedValue)}
}

func NewBlockScope(parent Scope) Scope {
	return &BlockScope{parent, make(map[string]TypedValue)}
}

func (s *BlockScope) Parent() Scope {
	return s.parent
}

func (s *BlockScope) Define(name string, v TypedValue) error {
	if _, exists := s.defs[name]; exists {
		return fmt.Errorf("name %s already exists", name)
	}
	s.defs[name] = v
	return nil
}

func (s *BlockScope) Lookup(name string) (TypedValue, bool) {
	if v, ok := s.defs[name]; ok {
		return v, true
	}
	return s.parent.Lookup(name)
}

func (s *BlockScope) String() string {
	b := strings.Builder{}
	b.WriteString(s.parent.String())
	for name, tv := range s.defs {
		fmt.Fprintf(&b, "\n%s : %s = %s", name, tv.Type, tv.Value)
	}
	return b.String()
}
