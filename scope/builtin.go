package scope

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
	"gitlab.com/davidjmacdonald1/lynx-compiler/value"
)

type BuiltinScope struct {
	defs map[string]TypedValue
}

func (s *BuiltinScope) Parent() Scope {
	panic("builtin does not have a parent scope")
}

func (s *BuiltinScope) Define(name string, v TypedValue) error {
	return fmt.Errorf("attempting to call Define on BuiltinScope")
}

func (s *BuiltinScope) Lookup(name string) (TypedValue, bool) {
	if v, ok := s.defs[name]; ok {
		return v, true
	}
	return TypedValue{}, false
}

func (s *BuiltinScope) String() string {
	return ""
}

var Builtins *BuiltinScope

func initBuiltins() {
	Builtins = &BuiltinScope{make(map[string]TypedValue)}

	initType("Type", typ.Type_)
	initType("Bool", typ.Bool)

	initType("Int8", typ.Int8)
	initType("Int16", typ.Int16)
	initType("Int32", typ.Int32)
	initType("Int64", typ.Int64)
	initType("Int", typ.Int)

	initType("Byte", typ.Byte)
	initType("UInt8", typ.UInt8)
	initType("UInt16", typ.UInt16)
	initType("UInt32", typ.UInt32)
	initType("UInt64", typ.UInt64)
	initType("UInt", typ.UInt)

	initType("Float32", typ.Float32)
	initType("Float64", typ.Float64)
	initType("Float", typ.Float)

	Builtins.defs["true"] = TypedValue{typ.UntypedBool, value.Bool{Val: true}}
	Builtins.defs["false"] = TypedValue{typ.UntypedBool, value.Bool{Val: false}}
}

func initType(name string, t typ.Type) {
	Builtins.defs[name] = TypedValue{typ.Type_, value.Type{Type: t}}
}
