package value

import (
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

type None struct{}

func (v None) IsConst() bool {
	return false
}

func (v None) ConvertTo(to typ.Type) (Value, error) {
	return v, nil
}

func (v None) MagnitudeOp() Value {
	return v
}

func (v None) PrefixOp(op string) Value {
	return v
}

func (v None) BinaryOp(op string, right Value) (Value, error) {
	switch op {
	case "/", "%":
		if t, ok := right.(NumericValue); ok && t.Sign() == 0 {
			return nil, divideByZeroErr()
		}
	case "<<", ">>":
		if t, ok := right.(NumericValue); ok && t.Sign() < 0 {
			return nil, invalidOpErr()
		}
	case "or":
		if t, ok := right.(Bool); ok && t.Val {
			return Bool{true}, nil
		}
	}
	return v, nil
}

func (v None) String() string {
	return "runtime value"
}
