package value

import (
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

var Unit = MakeType(typ.Unit)

type Type struct {
	Type typ.Type
}

func MakeType(val typ.Type) Type {
	return Type{val}
}

func (v Type) IsConst() bool {
	return true
}

func (v Type) ConvertTo(to typ.Type) (Value, error) {
	to = typ.BaseType(to)
	if to == typ.Type_ {
		return v, nil
	}
	panic(unexpectedTypeErr(to))
}

func (v Type) MagnitudeOp() Value {
	panic(invalidOpErr())
}

func (v Type) PrefixOp(op string) Value {
	panic(invalidOpErr())
}

func (v Type) BinaryOp(op string, right Value) (Value, error) {
	panic(invalidOpErr())
}

func (v Type) String() string {
	if v.Type.String() == "()" {
		return "({})"
	}

	return v.Type.String()
}
