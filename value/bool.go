package value

import (
	"fmt"
	"math/big"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

type Bool struct {
	Val bool
}

func MakeBool(val bool) Bool {
	return Bool{val}
}

func (v Bool) IsConst() bool {
	return true
}

func (v Bool) ConvertTo(to typ.Type) (Value, error) {
	to = typ.BaseType(to)
	switch to.(type) {
	case typ.BoolType:
		return v, nil
	case typ.IntType:
		if v.Val {
			return Int{big.NewInt(1)}, nil
		} else {
			return Int{big.NewInt(0)}, nil
		}
	case typ.FloatType:
		if v.Val {
			return Float{big.NewFloat(1)}, nil
		} else {
			return Float{big.NewFloat(0)}, nil
		}
	}
	panic(unexpectedTypeErr(to))
}

func (v Bool) MagnitudeOp() Value {
	if v.Val {
		return Int{big.NewInt(1)}
	}
	return Int{big.NewInt(0)}
}

func (v Bool) PrefixOp(op string) Value {
	switch op {
	case "not":
		v.Val = !v.Val
	default:
		panic(unexpectedOpErr(op, "Bool"))
	}
	return v
}

func (v Bool) BinaryOp(op string, right Value) (Value, error) {
	if op == "and" && !v.Val {
		return Bool{false}, nil
	}
	if !right.IsConst() {
		return None{}, nil
	}

	r := right.(Bool)
	switch op {
	case "and":
		v.Val = v.Val && r.Val
	case "or":
		v.Val = v.Val || r.Val
	case "==":
		v.Val = v.Val == r.Val
	case "!=":
		v.Val = v.Val != r.Val
	default:
		panic(unexpectedOpErr(op, "Bool"))
	}
	return v, nil
}

func (v Bool) String() string {
	return fmt.Sprintf("%t", v.Val)
}
