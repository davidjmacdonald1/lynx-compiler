package value

import (
	"math"
	"math/big"

	"github.com/ALTree/bigfloat"
	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

type Float struct {
	Val *big.Float
}

func MakeFloat(val float64) Float {
	return Float{big.NewFloat(val)}
}

func (v Float) Sign() int {
	return v.Val.Sign()
}

func (v Float) IsConst() bool {
	return true
}

func (v Float) ConvertTo(to typ.Type) (Value, error) {
	to = typ.BaseType(to)
	switch to.(type) {
	case typ.BoolType:
		return Bool{v.Sign() != 0}, nil
	case typ.IntType:
		if !v.Val.IsInt() {
			return nil, representErr(v, to)
		}
		val, _ := v.Val.Int(nil)
		i := Int{val}
		return i, i.ensureFits(to.(typ.IntType))
	case typ.FloatType:
		return v, v.ensureFits(to.(typ.FloatType))
	}
	panic(unexpectedTypeErr(to))
}

func (v Float) MagnitudeOp() Value {
	v.Val = big.NewFloat(0).Abs(v.Val)
	return v
}

func (v Float) PrefixOp(op string) Value {
	switch op {
	case "+":
	case "-":
		v.Val = big.NewFloat(0).Neg(v.Val)
	default:
		panic(unexpectedOpErr(op, "Float"))
	}
	return &v
}

func (v Float) BinaryOp(op string, right Value) (Value, error) {
	if !right.IsConst() {
		return None{}, nil
	}

	r := right.(Float)
	switch op {
	case "^":
		if v.Sign() == 0 && r.Sign() <= 0 {
			return nil, invalidExponentErr()
		}
		v.Val = bigfloat.Pow(v.Val, r.Val)
	case "*":
		v.Val = big.NewFloat(0).Mul(v.Val, r.Val)
	case "/":
		if r.Sign() == 0 {
			return nil, divideByZeroErr()
		}
		v.Val = big.NewFloat(0).Quo(v.Val, r.Val)
	case "%":
		if r.Sign() == 0 {
			return nil, divideByZeroErr()
		}

		quo, _ := big.NewFloat(0).Quo(v.Val, r.Val).Int(nil)
		prod := big.NewFloat(0).Mul(big.NewFloat(0).SetInt(quo), r.Val)
		v.Val = big.NewFloat(0).Sub(v.Val, prod)
	case "+":
		v.Val = big.NewFloat(0).Add(v.Val, r.Val)
	case "-":
		v.Val = big.NewFloat(0).Sub(v.Val, r.Val)
	case "<":
		return Bool{v.Val.Cmp(r.Val) < 0}, nil
	case "<=":
		return Bool{v.Val.Cmp(r.Val) <= 0}, nil
	case ">":
		return Bool{v.Val.Cmp(r.Val) > 0}, nil
	case ">=":
		return Bool{v.Val.Cmp(r.Val) >= 0}, nil
	case "==":
		return Bool{v.Val.Cmp(r.Val) == 0}, nil
	case "!=":
		return Bool{v.Val.Cmp(r.Val) != 0}, nil
	default:
		panic(unexpectedOpErr(op, "Float"))
	}
	return v, nil
}

func (v Float) String() string {
	return v.Val.String()
}

func (v Float) ensureFits(t typ.FloatType) error {
	if v.Sign() == 0 {
		return nil
	}

	var min, max *big.Float
	switch t {
	case typ.UntypedFloat:
		return nil
	case typ.Float32:
		min = big.NewFloat(math.SmallestNonzeroFloat32)
		max = big.NewFloat(math.MaxFloat32)
	case typ.Float64:
		min = big.NewFloat(math.SmallestNonzeroFloat64)
		max = big.NewFloat(math.MaxFloat64)
	default:
		panic(unexpectedTypeErr(t))
	}

	abs := big.NewFloat(0).Abs(v.Val)
	if min.Cmp(abs) > 0 || max.Cmp(abs) < 0 {
		return representErr(v, t)
	}
	return nil
}
