package value

import (
	"strings"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

type Tuple struct {
	Members []Value
}

func MakeTuple(members ...Value) Tuple {
	return Tuple{members}
}

func (v Tuple) IsConst() bool {
	for _, member := range v.Members {
		if !member.IsConst() {
			return false
		}
	}
	return true
}

func (v Tuple) ConvertTo(to typ.Type) (Value, error) {
	tuple, ok := typ.As[typ.TupleType](to)
	if !ok || len(tuple.Members) != len(v.Members) {
		return nil, unexpectedTypeErr(to)
	}

	members := make([]Value, len(v.Members))
	for i, member := range v.Members {
		to := typ.BaseType(tuple.Members[i])
		m, err := member.ConvertTo(to)
		if err != nil {
			return nil, err
		}
		members[i] = m
	}
	return Tuple{members}, nil
}

func (v Tuple) MagnitudeOp() Value {
	return MakeInt(int64(len(v.Members)))
}

func (v Tuple) PrefixOp(op string) Value {
	panic(unexpectedOpErr(op, "Tuple"))
}

func (v Tuple) BinaryOp(op string, right Value) (Value, error) {
	// TODO implement == and !=
	panic(unexpectedOpErr(op, "Tuple"))
}

func (v Tuple) String() string {
	b := strings.Builder{}
	b.WriteRune('(')
	for i, member := range v.Members {
		if i > 0 {
			b.WriteString(", ")
		}
		b.WriteString(member.String())
	}
	b.WriteRune(')')
	return b.String()
}
