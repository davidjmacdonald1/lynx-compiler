package value

import (
	"math"
	"math/big"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

type Int struct {
	Val *big.Int
}

func MakeInt(val int64) Int {
	return Int{big.NewInt(val)}
}

func (v Int) IsConst() bool {
	return true
}

func (v Int) Sign() int {
	return v.Val.Sign()
}

func (v Int) ConvertTo(to typ.Type) (Value, error) {
	to = typ.BaseType(to)
	switch to.(type) {
	case typ.BoolType:
		return Bool{v.Sign() != 0}, nil
	case typ.IntType:
		return v, v.ensureFits(to.(typ.IntType))
	case typ.FloatType:
		f := Float{big.NewFloat(0).SetInt(v.Val)}
		return f, f.ensureFits(to.(typ.FloatType))
	}
	panic(unexpectedTypeErr(to))
}

func (v Int) MagnitudeOp() Value {
	v.Val = big.NewInt(0).Abs(v.Val)
	return v
}

func (v Int) PrefixOp(op string) Value {
	switch op {
	case "+":
	case "-":
		v.Val = big.NewInt(0).Neg(v.Val)
	case "~":
		v.Val = big.NewInt(0).Not(v.Val)
	default:
		panic(unexpectedOpErr(op, "Int"))
	}
	return v
}

func (v Int) BinaryOp(op string, right Value) (Value, error) {
	if !right.IsConst() {
		return None{}, nil
	}

	r := right.(Int)
	switch op {
	case "^":
		if v.Sign() == 0 && r.Sign() <= 0 {
			return nil, invalidExponentErr()
		}
		if r.Sign() < 0 {
			v.Val = big.NewInt(0)
		} else {
			v.Val = big.NewInt(0).Exp(v.Val, r.Val, nil)
		}
	case "*":
		v.Val = big.NewInt(0).Mul(v.Val, r.Val)
	case "/":
		if r.Sign() == 0 {
			return nil, divideByZeroErr()
		}
		v.Val = big.NewInt(0).Div(v.Val, r.Val)
	case "%":
		if r.Sign() == 0 {
			return nil, divideByZeroErr()
		}
		v.Val = big.NewInt(0).Rem(v.Val, r.Val)
	case "+":
		v.Val = big.NewInt(0).Add(v.Val, r.Val)
	case "-":
		v.Val = big.NewInt(0).Sub(v.Val, r.Val)
	case "<<":
		if r.Sign() < 0 || r.Val.Cmp(big.NewInt(math.MaxUint32)) > 0 {
			return nil, invalidOpErr()
		}
		v.Val = big.NewInt(0).Lsh(v.Val, uint(r.Val.Uint64()))
	case ">>":
		if r.Sign() < 0 || r.Val.Cmp(big.NewInt(math.MaxUint32)) > 0 {
			return nil, invalidOpErr()
		}
		v.Val = big.NewInt(0).Rsh(v.Val, uint(r.Val.Uint64()))
	case "&":
		v.Val = big.NewInt(0).And(v.Val, r.Val)
	case "~":
		v.Val = big.NewInt(0).Xor(v.Val, r.Val)
	case "|":
		v.Val = big.NewInt(0).Or(v.Val, r.Val)
	case "<":
		return Bool{v.Val.Cmp(r.Val) < 0}, nil
	case "<=":
		return Bool{v.Val.Cmp(r.Val) <= 0}, nil
	case ">":
		return Bool{v.Val.Cmp(r.Val) > 0}, nil
	case ">=":
		return Bool{v.Val.Cmp(r.Val) >= 0}, nil
	case "==":
		return Bool{v.Val.Cmp(r.Val) == 0}, nil
	case "!=":
		return Bool{v.Val.Cmp(r.Val) != 0}, nil
	default:
		panic(unexpectedOpErr(op, "Int"))
	}
	return v, nil
}

func (v Int) String() string {
	return v.Val.String()
}

func (v Int) ensureFits(t typ.IntType) error {
	var max *big.Int
	min := big.NewInt(0)
	switch t {
	case typ.UntypedInt:
		return nil
	case typ.Int8:
		min, max = big.NewInt(math.MinInt8), big.NewInt(math.MaxInt8)
	case typ.Int16:
		min, max = big.NewInt(math.MinInt16), big.NewInt(math.MaxInt16)
	case typ.Int32:
		min, max = big.NewInt(math.MinInt32), big.NewInt(math.MaxInt32)
	case typ.Int64:
		min, max = big.NewInt(math.MinInt64), big.NewInt(math.MaxInt64)
	case typ.UInt8:
		max = big.NewInt(math.MaxUint8)
	case typ.UInt16:
		max = big.NewInt(math.MaxUint16)
	case typ.UInt32:
		max = big.NewInt(math.MaxUint32)
	case typ.UInt64:
		max = big.NewInt(0).SetUint64(math.MaxUint64)
	default:
		panic(unexpectedTypeErr(t))
	}

	if min.Cmp(v.Val) > 0 || max.Cmp(v.Val) < 0 {
		return representErr(v, t)
	}
	return nil
}
