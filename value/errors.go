package value

import (
	"fmt"

	"gitlab.com/davidjmacdonald1/lynx-compiler/typ"
)

func representErr(v Value, t typ.Type) error {
	return fmt.Errorf("value %s cannot be fully represented by %s", v, t)
}

func divideByZeroErr() error {
	return fmt.Errorf("cannot divide by 0")
}

func invalidExponentErr() error {
	return fmt.Errorf("0^a for a <= 0 is undefined")
}

func invalidOpErr() error {
	return fmt.Errorf("invalid operation")
}

func unexpectedOpErr(op, typeName string) error {
	return fmt.Errorf("unexpected op %s for type %s", op, typeName)
}

func unexpectedTypeErr(t typ.Type) error {
	return fmt.Errorf("unexpected type %s", t)
}
