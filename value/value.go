package value

import "gitlab.com/davidjmacdonald1/lynx-compiler/typ"

type Value interface {
	IsConst() bool
	ConvertTo(to typ.Type) (Value, error)
	MagnitudeOp() Value
	PrefixOp(op string) Value
	BinaryOp(op string, right Value) (Value, error)
	String() string
}

type NumericValue interface {
	Sign() int
}

func BinaryOpAs(as typ.Type, op string, left, right Value) (Value, error) {
	left, err := left.ConvertTo(as)
	if err != nil {
		return nil, err
	}

	right, err = right.ConvertTo(as)
	if err != nil {
		return nil, err
	}

	return left.BinaryOp(op, right)
}
