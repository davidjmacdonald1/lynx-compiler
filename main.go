package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/exec"
	"path"
	"strings"
	"sync"

	"gitlab.com/davidjmacdonald1/lynx-compiler/node"
)

var debug, run bool

func main() {
	flag.BoolVar(&debug, "debug", false, "writes the C and ASM to files")
	flag.BoolVar(&run, "run", false, "runs the generated executable")
	pprof := flag.Bool("pprof", false, "runs a pprof test")
	flag.Parse()

	if *pprof {
		pprofTest()
		return
	}

	if len(flag.Args()) == 0 {
		runRepl()
	} else {
		buildFile(flag.Args()[0])
	}
}

func runRepl() {
	fmt.Println("Lynx Compiler")
	stdin := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print(">>> ")
		if !stdin.Scan() {
			return
		}

		line := strings.TrimSpace(stdin.Text())
		for strings.HasSuffix(line, "\\") {
			fmt.Print("... ")
			if !stdin.Scan() {
				return
			}
			line = strings.TrimSuffix(line, "\\") + "\n" + stdin.Text()
		}

		binFile, stdout := build("main", line)
		if binFile == "" {
			continue
		}

		if run {
			output, err := execute(binFile)
			if err != nil {
				fmt.Println("Execution failed:", err)
				continue
			}
			fmt.Print(output)
		} else {
			fmt.Print(stdout)
		}
	}
}

func buildFile(file string) {
	binFile, _ := build(file, "")
	if binFile == "" {
		return
	}

	if run {
		output, err := execute(binFile)
		if err != nil {
			fmt.Println("Execution failed:", err)
			return
		}
		fmt.Print(output)
	} else {
		fmt.Printf("Created %s\n", binFile)
	}
}

var text string

func pprofTest() {
	header := "val num = 1; val cond = false;"
	source := `
		|Float(num) + 2^.5|
		|1|2 - 1| / num
		print(not 0 <= 3 < 4 and cond)
	`
	source = header + strings.Repeat(source, 10_000)

	var wg sync.WaitGroup
	go func() {
		fmt.Println("Starting pprof on port 8080")
		http.ListenAndServe("localhost:8080", nil)
	}()

	wg.Add(1)
	for range 10 {
		tree, err := node.ParseString(source)
		if err != nil {
			panic(err)
		}

		err = node.CheckTree(tree)
		if err != nil {
			panic(err)
		}

		_, text = node.EmitTree(tree)
	}
	fmt.Println("Done! Exit with Ctrl+c")
	wg.Wait()
}

func build(srcName string, stdin string) (binFile string, stdout string) {
	var tree node.Node
	var err error

	if stdin == "" {
		tree, err = node.ParseFile(srcName)
	} else {
		tree, err = node.ParseString(stdin)
	}
	if err != nil {
		fmt.Println("Parsing failed:", err)
		return "", ""
	}

	err = node.CheckTree(tree)
	if err != nil {
		fmt.Println("Checking failed:", err)
		return "", ""
	}

	err = exec.Command("mkdir", "-p", "tmp").Run()
	if err != nil {
		fmt.Println("Making temporary directory failed:", err)
		return "", ""
	}

	header, stdout := node.EmitTree(tree)
	binFile, err = gcc(srcName, header, stdout)
	if err != nil {
		fmt.Println("GCC failed:", err)
		return "", ""
	}
	return binFile, stdout
}

func gcc(file, header, stdin string) (binFile string, err error) {
	stdin = fmt.Sprintf("%sint main() {\n%s    return 0;\n}\n", header, stdin)
	err = exec.Command("mkdir", "-p", "tmp").Run()
	if err != nil {
		return file, err
	}

	file = "tmp/" + strings.TrimSuffix(path.Base(file), ".lynx")
	args := []string{
		"-o", file, "-xc", "-fmax-errors=5", "-Wall", "-O2", "-", "-lm",
	}
	if debug {
		args = append([]string{"-save-temps"}, args...)
		err := os.WriteFile(file+".c", []byte(stdin), 0o644)
		if err != nil {
			return "", err
		}
	}

	gcc := exec.Command("gcc", args...)

	var buf bytes.Buffer
	gcc.Stderr = &buf
	gcc.Stdin = bytes.NewBufferString(stdin)

	err = gcc.Run()
	if err != nil {
		err = fmt.Errorf("%s:\n%s", err, buf.String())
	} else if buf.Len() != 0 {
		fmt.Printf("GCC Warning:\n %s", buf.String())
	}
	return file, err
}

func execute(name string) (string, error) {
	out, err := exec.Command("./" + name).Output()
	if err != nil {
		return "", err
	}
	return string(out), nil
}
