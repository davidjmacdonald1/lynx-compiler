module gitlab.com/davidjmacdonald1/lynx-compiler

go 1.22.0

require github.com/antlr4-go/antlr/v4 v4.13.0

require github.com/ALTree/bigfloat v0.2.0

require golang.org/x/exp v0.0.0-20230515195305-f3d0a9c9a5cc // indirect
