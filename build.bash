#!/bin/bash

antlr -Dlanguage=Go LynxLexer.g4 -o parser
antlr -Dlanguage=Go LynxParser.g4 -visitor -o parser
