// Code generated from LynxParser.g4 by ANTLR 4.13.2. DO NOT EDIT.

package parser // LynxParser

import "github.com/antlr4-go/antlr/v4"

// LynxParserListener is a complete listener for a parse tree produced by LynxParser.
type LynxParserListener interface {
	antlr.ParseTreeListener

	// EnterFileStat is called when entering the fileStat production.
	EnterFileStat(c *FileStatContext)

	// EnterEndedStat is called when entering the endedStat production.
	EnterEndedStat(c *EndedStatContext)

	// EnterLineEnd is called when entering the lineEnd production.
	EnterLineEnd(c *LineEndContext)

	// EnterMultiStat is called when entering the multiStat production.
	EnterMultiStat(c *MultiStatContext)

	// EnterConstStat is called when entering the constStat production.
	EnterConstStat(c *ConstStatContext)

	// EnterDeclStat is called when entering the declStat production.
	EnterDeclStat(c *DeclStatContext)

	// EnterPrintStat is called when entering the printStat production.
	EnterPrintStat(c *PrintStatContext)

	// EnterExprStat is called when entering the exprStat production.
	EnterExprStat(c *ExprStatContext)

	// EnterTupleElem is called when entering the tupleElem production.
	EnterTupleElem(c *TupleElemContext)

	// EnterMultiStat_ is called when entering the multiStat_ production.
	EnterMultiStat_(c *MultiStat_Context)

	// EnterOrExpr is called when entering the orExpr production.
	EnterOrExpr(c *OrExprContext)

	// EnterAddrExpr is called when entering the addrExpr production.
	EnterAddrExpr(c *AddrExprContext)

	// EnterNumExpr is called when entering the numExpr production.
	EnterNumExpr(c *NumExprContext)

	// EnterParenExpr is called when entering the parenExpr production.
	EnterParenExpr(c *ParenExprContext)

	// EnterBitXorExpr is called when entering the bitXorExpr production.
	EnterBitXorExpr(c *BitXorExprContext)

	// EnterShiftExpr is called when entering the shiftExpr production.
	EnterShiftExpr(c *ShiftExprContext)

	// EnterMagExpr is called when entering the magExpr production.
	EnterMagExpr(c *MagExprContext)

	// EnterPrefixExpr is called when entering the prefixExpr production.
	EnterPrefixExpr(c *PrefixExprContext)

	// EnterBitOrExpr is called when entering the bitOrExpr production.
	EnterBitOrExpr(c *BitOrExprContext)

	// EnterNotExpr is called when entering the notExpr production.
	EnterNotExpr(c *NotExprContext)

	// EnterAddExpr is called when entering the addExpr production.
	EnterAddExpr(c *AddExprContext)

	// EnterCompExpr is called when entering the compExpr production.
	EnterCompExpr(c *CompExprContext)

	// EnterDerefExpr is called when entering the derefExpr production.
	EnterDerefExpr(c *DerefExprContext)

	// EnterMulExpr is called when entering the mulExpr production.
	EnterMulExpr(c *MulExprContext)

	// EnterReassignExpr is called when entering the reassignExpr production.
	EnterReassignExpr(c *ReassignExprContext)

	// EnterIfExpr is called when entering the ifExpr production.
	EnterIfExpr(c *IfExprContext)

	// EnterTupleExpr is called when entering the tupleExpr production.
	EnterTupleExpr(c *TupleExprContext)

	// EnterPowExpr is called when entering the powExpr production.
	EnterPowExpr(c *PowExprContext)

	// EnterBitAndExpr is called when entering the bitAndExpr production.
	EnterBitAndExpr(c *BitAndExprContext)

	// EnterCallExpr is called when entering the callExpr production.
	EnterCallExpr(c *CallExprContext)

	// EnterIdExpr is called when entering the idExpr production.
	EnterIdExpr(c *IdExprContext)

	// EnterPointerTypeExpr is called when entering the pointerTypeExpr production.
	EnterPointerTypeExpr(c *PointerTypeExprContext)

	// EnterAndExpr is called when entering the andExpr production.
	EnterAndExpr(c *AndExprContext)

	// ExitFileStat is called when exiting the fileStat production.
	ExitFileStat(c *FileStatContext)

	// ExitEndedStat is called when exiting the endedStat production.
	ExitEndedStat(c *EndedStatContext)

	// ExitLineEnd is called when exiting the lineEnd production.
	ExitLineEnd(c *LineEndContext)

	// ExitMultiStat is called when exiting the multiStat production.
	ExitMultiStat(c *MultiStatContext)

	// ExitConstStat is called when exiting the constStat production.
	ExitConstStat(c *ConstStatContext)

	// ExitDeclStat is called when exiting the declStat production.
	ExitDeclStat(c *DeclStatContext)

	// ExitPrintStat is called when exiting the printStat production.
	ExitPrintStat(c *PrintStatContext)

	// ExitExprStat is called when exiting the exprStat production.
	ExitExprStat(c *ExprStatContext)

	// ExitTupleElem is called when exiting the tupleElem production.
	ExitTupleElem(c *TupleElemContext)

	// ExitMultiStat_ is called when exiting the multiStat_ production.
	ExitMultiStat_(c *MultiStat_Context)

	// ExitOrExpr is called when exiting the orExpr production.
	ExitOrExpr(c *OrExprContext)

	// ExitAddrExpr is called when exiting the addrExpr production.
	ExitAddrExpr(c *AddrExprContext)

	// ExitNumExpr is called when exiting the numExpr production.
	ExitNumExpr(c *NumExprContext)

	// ExitParenExpr is called when exiting the parenExpr production.
	ExitParenExpr(c *ParenExprContext)

	// ExitBitXorExpr is called when exiting the bitXorExpr production.
	ExitBitXorExpr(c *BitXorExprContext)

	// ExitShiftExpr is called when exiting the shiftExpr production.
	ExitShiftExpr(c *ShiftExprContext)

	// ExitMagExpr is called when exiting the magExpr production.
	ExitMagExpr(c *MagExprContext)

	// ExitPrefixExpr is called when exiting the prefixExpr production.
	ExitPrefixExpr(c *PrefixExprContext)

	// ExitBitOrExpr is called when exiting the bitOrExpr production.
	ExitBitOrExpr(c *BitOrExprContext)

	// ExitNotExpr is called when exiting the notExpr production.
	ExitNotExpr(c *NotExprContext)

	// ExitAddExpr is called when exiting the addExpr production.
	ExitAddExpr(c *AddExprContext)

	// ExitCompExpr is called when exiting the compExpr production.
	ExitCompExpr(c *CompExprContext)

	// ExitDerefExpr is called when exiting the derefExpr production.
	ExitDerefExpr(c *DerefExprContext)

	// ExitMulExpr is called when exiting the mulExpr production.
	ExitMulExpr(c *MulExprContext)

	// ExitReassignExpr is called when exiting the reassignExpr production.
	ExitReassignExpr(c *ReassignExprContext)

	// ExitIfExpr is called when exiting the ifExpr production.
	ExitIfExpr(c *IfExprContext)

	// ExitTupleExpr is called when exiting the tupleExpr production.
	ExitTupleExpr(c *TupleExprContext)

	// ExitPowExpr is called when exiting the powExpr production.
	ExitPowExpr(c *PowExprContext)

	// ExitBitAndExpr is called when exiting the bitAndExpr production.
	ExitBitAndExpr(c *BitAndExprContext)

	// ExitCallExpr is called when exiting the callExpr production.
	ExitCallExpr(c *CallExprContext)

	// ExitIdExpr is called when exiting the idExpr production.
	ExitIdExpr(c *IdExprContext)

	// ExitPointerTypeExpr is called when exiting the pointerTypeExpr production.
	ExitPointerTypeExpr(c *PointerTypeExprContext)

	// ExitAndExpr is called when exiting the andExpr production.
	ExitAndExpr(c *AndExprContext)
}
