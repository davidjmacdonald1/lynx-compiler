package parser

import (
	"fmt"

	"github.com/antlr4-go/antlr/v4"
)

func Parse(input *antlr.InputStream) (tree IFileStatContext, err error) {
	defer func() {
		if r := recover(); r != nil {
			if rErr, ok := r.(*syntaxError); ok {
				tree, err = nil, rErr
			} else {
				panic(r)
			}
		}
	}()

	lexer := NewLynxLexer(input)
	lexer.RemoveErrorListeners()
	lexer.AddErrorListener(newLynxErrorListener())

	tokens := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
	parser := NewLynxParser(tokens)
	parser.RemoveErrorListeners()
	parser.AddErrorListener(newLynxErrorListener())

	return parser.FileStat(), nil
}

func ParseString(input string) (antlr.ParseTree, error) {
	return Parse(antlr.NewInputStream(input))
}

func ParseFile(name string) (antlr.ParseTree, error) {
	fs, err := antlr.NewFileStream(name)
	if err != nil {
		return nil, err
	}
	return Parse(&fs.InputStream)
}

type syntaxError struct {
	line, col int
	msg       string
}

func (e *syntaxError) Error() string {
	return fmt.Sprintf("Syntax error at line %d:%d - %s", e.line, e.col, e.msg)
}

type lynxErrorListener struct {
	*antlr.DefaultErrorListener
	err error
}

func newLynxErrorListener() *lynxErrorListener {
	return &lynxErrorListener{
		DefaultErrorListener: antlr.NewDefaultErrorListener(),
	}
}

func (l *lynxErrorListener) SyntaxError(
	recognizer antlr.Recognizer,
	offendingSymbol any,
	line, col int,
	msg string,
	ex antlr.RecognitionException,
) {
	panic(&syntaxError{line, col, msg})
}
