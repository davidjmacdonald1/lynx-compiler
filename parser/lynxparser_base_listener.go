// Code generated from LynxParser.g4 by ANTLR 4.13.2. DO NOT EDIT.

package parser // LynxParser

import "github.com/antlr4-go/antlr/v4"

// BaseLynxParserListener is a complete listener for a parse tree produced by LynxParser.
type BaseLynxParserListener struct{}

var _ LynxParserListener = &BaseLynxParserListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseLynxParserListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseLynxParserListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseLynxParserListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseLynxParserListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterFileStat is called when production fileStat is entered.
func (s *BaseLynxParserListener) EnterFileStat(ctx *FileStatContext) {}

// ExitFileStat is called when production fileStat is exited.
func (s *BaseLynxParserListener) ExitFileStat(ctx *FileStatContext) {}

// EnterEndedStat is called when production endedStat is entered.
func (s *BaseLynxParserListener) EnterEndedStat(ctx *EndedStatContext) {}

// ExitEndedStat is called when production endedStat is exited.
func (s *BaseLynxParserListener) ExitEndedStat(ctx *EndedStatContext) {}

// EnterLineEnd is called when production lineEnd is entered.
func (s *BaseLynxParserListener) EnterLineEnd(ctx *LineEndContext) {}

// ExitLineEnd is called when production lineEnd is exited.
func (s *BaseLynxParserListener) ExitLineEnd(ctx *LineEndContext) {}

// EnterMultiStat is called when production multiStat is entered.
func (s *BaseLynxParserListener) EnterMultiStat(ctx *MultiStatContext) {}

// ExitMultiStat is called when production multiStat is exited.
func (s *BaseLynxParserListener) ExitMultiStat(ctx *MultiStatContext) {}

// EnterConstStat is called when production constStat is entered.
func (s *BaseLynxParserListener) EnterConstStat(ctx *ConstStatContext) {}

// ExitConstStat is called when production constStat is exited.
func (s *BaseLynxParserListener) ExitConstStat(ctx *ConstStatContext) {}

// EnterDeclStat is called when production declStat is entered.
func (s *BaseLynxParserListener) EnterDeclStat(ctx *DeclStatContext) {}

// ExitDeclStat is called when production declStat is exited.
func (s *BaseLynxParserListener) ExitDeclStat(ctx *DeclStatContext) {}

// EnterPrintStat is called when production printStat is entered.
func (s *BaseLynxParserListener) EnterPrintStat(ctx *PrintStatContext) {}

// ExitPrintStat is called when production printStat is exited.
func (s *BaseLynxParserListener) ExitPrintStat(ctx *PrintStatContext) {}

// EnterExprStat is called when production exprStat is entered.
func (s *BaseLynxParserListener) EnterExprStat(ctx *ExprStatContext) {}

// ExitExprStat is called when production exprStat is exited.
func (s *BaseLynxParserListener) ExitExprStat(ctx *ExprStatContext) {}

// EnterTupleElem is called when production tupleElem is entered.
func (s *BaseLynxParserListener) EnterTupleElem(ctx *TupleElemContext) {}

// ExitTupleElem is called when production tupleElem is exited.
func (s *BaseLynxParserListener) ExitTupleElem(ctx *TupleElemContext) {}

// EnterMultiStat_ is called when production multiStat_ is entered.
func (s *BaseLynxParserListener) EnterMultiStat_(ctx *MultiStat_Context) {}

// ExitMultiStat_ is called when production multiStat_ is exited.
func (s *BaseLynxParserListener) ExitMultiStat_(ctx *MultiStat_Context) {}

// EnterOrExpr is called when production orExpr is entered.
func (s *BaseLynxParserListener) EnterOrExpr(ctx *OrExprContext) {}

// ExitOrExpr is called when production orExpr is exited.
func (s *BaseLynxParserListener) ExitOrExpr(ctx *OrExprContext) {}

// EnterAddrExpr is called when production addrExpr is entered.
func (s *BaseLynxParserListener) EnterAddrExpr(ctx *AddrExprContext) {}

// ExitAddrExpr is called when production addrExpr is exited.
func (s *BaseLynxParserListener) ExitAddrExpr(ctx *AddrExprContext) {}

// EnterNumExpr is called when production numExpr is entered.
func (s *BaseLynxParserListener) EnterNumExpr(ctx *NumExprContext) {}

// ExitNumExpr is called when production numExpr is exited.
func (s *BaseLynxParserListener) ExitNumExpr(ctx *NumExprContext) {}

// EnterParenExpr is called when production parenExpr is entered.
func (s *BaseLynxParserListener) EnterParenExpr(ctx *ParenExprContext) {}

// ExitParenExpr is called when production parenExpr is exited.
func (s *BaseLynxParserListener) ExitParenExpr(ctx *ParenExprContext) {}

// EnterBitXorExpr is called when production bitXorExpr is entered.
func (s *BaseLynxParserListener) EnterBitXorExpr(ctx *BitXorExprContext) {}

// ExitBitXorExpr is called when production bitXorExpr is exited.
func (s *BaseLynxParserListener) ExitBitXorExpr(ctx *BitXorExprContext) {}

// EnterShiftExpr is called when production shiftExpr is entered.
func (s *BaseLynxParserListener) EnterShiftExpr(ctx *ShiftExprContext) {}

// ExitShiftExpr is called when production shiftExpr is exited.
func (s *BaseLynxParserListener) ExitShiftExpr(ctx *ShiftExprContext) {}

// EnterMagExpr is called when production magExpr is entered.
func (s *BaseLynxParserListener) EnterMagExpr(ctx *MagExprContext) {}

// ExitMagExpr is called when production magExpr is exited.
func (s *BaseLynxParserListener) ExitMagExpr(ctx *MagExprContext) {}

// EnterPrefixExpr is called when production prefixExpr is entered.
func (s *BaseLynxParserListener) EnterPrefixExpr(ctx *PrefixExprContext) {}

// ExitPrefixExpr is called when production prefixExpr is exited.
func (s *BaseLynxParserListener) ExitPrefixExpr(ctx *PrefixExprContext) {}

// EnterBitOrExpr is called when production bitOrExpr is entered.
func (s *BaseLynxParserListener) EnterBitOrExpr(ctx *BitOrExprContext) {}

// ExitBitOrExpr is called when production bitOrExpr is exited.
func (s *BaseLynxParserListener) ExitBitOrExpr(ctx *BitOrExprContext) {}

// EnterNotExpr is called when production notExpr is entered.
func (s *BaseLynxParserListener) EnterNotExpr(ctx *NotExprContext) {}

// ExitNotExpr is called when production notExpr is exited.
func (s *BaseLynxParserListener) ExitNotExpr(ctx *NotExprContext) {}

// EnterAddExpr is called when production addExpr is entered.
func (s *BaseLynxParserListener) EnterAddExpr(ctx *AddExprContext) {}

// ExitAddExpr is called when production addExpr is exited.
func (s *BaseLynxParserListener) ExitAddExpr(ctx *AddExprContext) {}

// EnterCompExpr is called when production compExpr is entered.
func (s *BaseLynxParserListener) EnterCompExpr(ctx *CompExprContext) {}

// ExitCompExpr is called when production compExpr is exited.
func (s *BaseLynxParserListener) ExitCompExpr(ctx *CompExprContext) {}

// EnterDerefExpr is called when production derefExpr is entered.
func (s *BaseLynxParserListener) EnterDerefExpr(ctx *DerefExprContext) {}

// ExitDerefExpr is called when production derefExpr is exited.
func (s *BaseLynxParserListener) ExitDerefExpr(ctx *DerefExprContext) {}

// EnterMulExpr is called when production mulExpr is entered.
func (s *BaseLynxParserListener) EnterMulExpr(ctx *MulExprContext) {}

// ExitMulExpr is called when production mulExpr is exited.
func (s *BaseLynxParserListener) ExitMulExpr(ctx *MulExprContext) {}

// EnterReassignExpr is called when production reassignExpr is entered.
func (s *BaseLynxParserListener) EnterReassignExpr(ctx *ReassignExprContext) {}

// ExitReassignExpr is called when production reassignExpr is exited.
func (s *BaseLynxParserListener) ExitReassignExpr(ctx *ReassignExprContext) {}

// EnterIfExpr is called when production ifExpr is entered.
func (s *BaseLynxParserListener) EnterIfExpr(ctx *IfExprContext) {}

// ExitIfExpr is called when production ifExpr is exited.
func (s *BaseLynxParserListener) ExitIfExpr(ctx *IfExprContext) {}

// EnterTupleExpr is called when production tupleExpr is entered.
func (s *BaseLynxParserListener) EnterTupleExpr(ctx *TupleExprContext) {}

// ExitTupleExpr is called when production tupleExpr is exited.
func (s *BaseLynxParserListener) ExitTupleExpr(ctx *TupleExprContext) {}

// EnterPowExpr is called when production powExpr is entered.
func (s *BaseLynxParserListener) EnterPowExpr(ctx *PowExprContext) {}

// ExitPowExpr is called when production powExpr is exited.
func (s *BaseLynxParserListener) ExitPowExpr(ctx *PowExprContext) {}

// EnterBitAndExpr is called when production bitAndExpr is entered.
func (s *BaseLynxParserListener) EnterBitAndExpr(ctx *BitAndExprContext) {}

// ExitBitAndExpr is called when production bitAndExpr is exited.
func (s *BaseLynxParserListener) ExitBitAndExpr(ctx *BitAndExprContext) {}

// EnterCallExpr is called when production callExpr is entered.
func (s *BaseLynxParserListener) EnterCallExpr(ctx *CallExprContext) {}

// ExitCallExpr is called when production callExpr is exited.
func (s *BaseLynxParserListener) ExitCallExpr(ctx *CallExprContext) {}

// EnterIdExpr is called when production idExpr is entered.
func (s *BaseLynxParserListener) EnterIdExpr(ctx *IdExprContext) {}

// ExitIdExpr is called when production idExpr is exited.
func (s *BaseLynxParserListener) ExitIdExpr(ctx *IdExprContext) {}

// EnterPointerTypeExpr is called when production pointerTypeExpr is entered.
func (s *BaseLynxParserListener) EnterPointerTypeExpr(ctx *PointerTypeExprContext) {}

// ExitPointerTypeExpr is called when production pointerTypeExpr is exited.
func (s *BaseLynxParserListener) ExitPointerTypeExpr(ctx *PointerTypeExprContext) {}

// EnterAndExpr is called when production andExpr is entered.
func (s *BaseLynxParserListener) EnterAndExpr(ctx *AndExprContext) {}

// ExitAndExpr is called when production andExpr is exited.
func (s *BaseLynxParserListener) ExitAndExpr(ctx *AndExprContext) {}
