// Code generated from LynxParser.g4 by ANTLR 4.13.2. DO NOT EDIT.

package parser // LynxParser

import "github.com/antlr4-go/antlr/v4"

// A complete Visitor for a parse tree produced by LynxParser.
type LynxParserVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by LynxParser#fileStat.
	VisitFileStat(ctx *FileStatContext) interface{}

	// Visit a parse tree produced by LynxParser#endedStat.
	VisitEndedStat(ctx *EndedStatContext) interface{}

	// Visit a parse tree produced by LynxParser#lineEnd.
	VisitLineEnd(ctx *LineEndContext) interface{}

	// Visit a parse tree produced by LynxParser#multiStat.
	VisitMultiStat(ctx *MultiStatContext) interface{}

	// Visit a parse tree produced by LynxParser#constStat.
	VisitConstStat(ctx *ConstStatContext) interface{}

	// Visit a parse tree produced by LynxParser#declStat.
	VisitDeclStat(ctx *DeclStatContext) interface{}

	// Visit a parse tree produced by LynxParser#printStat.
	VisitPrintStat(ctx *PrintStatContext) interface{}

	// Visit a parse tree produced by LynxParser#exprStat.
	VisitExprStat(ctx *ExprStatContext) interface{}

	// Visit a parse tree produced by LynxParser#tupleElem.
	VisitTupleElem(ctx *TupleElemContext) interface{}

	// Visit a parse tree produced by LynxParser#multiStat_.
	VisitMultiStat_(ctx *MultiStat_Context) interface{}

	// Visit a parse tree produced by LynxParser#orExpr.
	VisitOrExpr(ctx *OrExprContext) interface{}

	// Visit a parse tree produced by LynxParser#addrExpr.
	VisitAddrExpr(ctx *AddrExprContext) interface{}

	// Visit a parse tree produced by LynxParser#numExpr.
	VisitNumExpr(ctx *NumExprContext) interface{}

	// Visit a parse tree produced by LynxParser#parenExpr.
	VisitParenExpr(ctx *ParenExprContext) interface{}

	// Visit a parse tree produced by LynxParser#bitXorExpr.
	VisitBitXorExpr(ctx *BitXorExprContext) interface{}

	// Visit a parse tree produced by LynxParser#shiftExpr.
	VisitShiftExpr(ctx *ShiftExprContext) interface{}

	// Visit a parse tree produced by LynxParser#magExpr.
	VisitMagExpr(ctx *MagExprContext) interface{}

	// Visit a parse tree produced by LynxParser#prefixExpr.
	VisitPrefixExpr(ctx *PrefixExprContext) interface{}

	// Visit a parse tree produced by LynxParser#bitOrExpr.
	VisitBitOrExpr(ctx *BitOrExprContext) interface{}

	// Visit a parse tree produced by LynxParser#notExpr.
	VisitNotExpr(ctx *NotExprContext) interface{}

	// Visit a parse tree produced by LynxParser#addExpr.
	VisitAddExpr(ctx *AddExprContext) interface{}

	// Visit a parse tree produced by LynxParser#compExpr.
	VisitCompExpr(ctx *CompExprContext) interface{}

	// Visit a parse tree produced by LynxParser#derefExpr.
	VisitDerefExpr(ctx *DerefExprContext) interface{}

	// Visit a parse tree produced by LynxParser#mulExpr.
	VisitMulExpr(ctx *MulExprContext) interface{}

	// Visit a parse tree produced by LynxParser#reassignExpr.
	VisitReassignExpr(ctx *ReassignExprContext) interface{}

	// Visit a parse tree produced by LynxParser#ifExpr.
	VisitIfExpr(ctx *IfExprContext) interface{}

	// Visit a parse tree produced by LynxParser#tupleExpr.
	VisitTupleExpr(ctx *TupleExprContext) interface{}

	// Visit a parse tree produced by LynxParser#powExpr.
	VisitPowExpr(ctx *PowExprContext) interface{}

	// Visit a parse tree produced by LynxParser#bitAndExpr.
	VisitBitAndExpr(ctx *BitAndExprContext) interface{}

	// Visit a parse tree produced by LynxParser#callExpr.
	VisitCallExpr(ctx *CallExprContext) interface{}

	// Visit a parse tree produced by LynxParser#idExpr.
	VisitIdExpr(ctx *IdExprContext) interface{}

	// Visit a parse tree produced by LynxParser#pointerTypeExpr.
	VisitPointerTypeExpr(ctx *PointerTypeExprContext) interface{}

	// Visit a parse tree produced by LynxParser#andExpr.
	VisitAndExpr(ctx *AndExprContext) interface{}
}
