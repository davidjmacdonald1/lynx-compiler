// Code generated from LynxParser.g4 by ANTLR 4.13.2. DO NOT EDIT.

package parser // LynxParser

import (
	"fmt"
	"strconv"
	"sync"

	"github.com/antlr4-go/antlr/v4"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = strconv.Itoa
var _ = sync.Once{}

type LynxParser struct {
	*antlr.BaseParser
}

var LynxParserParserStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	LiteralNames           []string
	SymbolicNames          []string
	RuleNames              []string
	PredictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func lynxparserParserInit() {
	staticData := &LynxParserParserStaticData
	staticData.LiteralNames = []string{
		"", "'print'", "'const'", "'var'", "'val'", "'='", "'else'", "'if'",
		"'or'", "'and'", "'not'", "'<'", "'>'", "'<='", "'>='", "'=='", "'!='",
		"'|'", "'~'", "'&'", "'<<'", "'>>'", "'%'", "'/'", "'*'", "'^'", "'+'",
		"'-'", "'.'", "'('", "')'", "'{'", "'}'", "','", "';'",
	}
	staticData.SymbolicNames = []string{
		"", "PRINT", "CONST", "VAR", "VAL", "ASSIGN", "ELSE", "IF", "OR", "AND",
		"NOT", "LT", "GT", "LTE", "GTE", "EQ", "NE", "BITOR", "BITXOR", "BITAND",
		"LSHIFT", "RSHIFT", "PERCENT", "SLASH", "STAR", "CARET", "PLUS", "MINUS",
		"DOT", "LPAREN", "RPAREN", "LBRACE", "RBRACE", "COMMA", "SEMICOLON",
		"NL", "IGNORED", "NUM", "ID",
	}
	staticData.RuleNames = []string{
		"fileStat", "endedStat", "lineEnd", "multiStat", "stat", "tupleElem",
		"expr",
	}
	staticData.PredictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 1, 38, 261, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2, 4, 7,
		4, 2, 5, 7, 5, 2, 6, 7, 6, 1, 0, 4, 0, 16, 8, 0, 11, 0, 12, 0, 17, 1, 0,
		3, 0, 21, 8, 0, 1, 1, 3, 1, 24, 8, 1, 1, 1, 1, 1, 1, 1, 1, 2, 4, 2, 30,
		8, 2, 11, 2, 12, 2, 31, 1, 2, 3, 2, 35, 8, 2, 1, 3, 1, 3, 3, 3, 39, 8,
		3, 1, 3, 1, 3, 1, 3, 5, 3, 44, 8, 3, 10, 3, 12, 3, 47, 9, 3, 1, 3, 3, 3,
		50, 8, 3, 1, 3, 3, 3, 53, 8, 3, 1, 3, 1, 3, 1, 4, 1, 4, 1, 4, 3, 4, 60,
		8, 4, 1, 4, 1, 4, 1, 4, 1, 4, 1, 4, 3, 4, 67, 8, 4, 1, 4, 1, 4, 1, 4, 1,
		4, 1, 4, 3, 4, 74, 8, 4, 1, 4, 1, 4, 3, 4, 78, 8, 4, 1, 4, 1, 4, 1, 4,
		3, 4, 83, 8, 4, 1, 5, 3, 5, 86, 8, 5, 1, 5, 1, 5, 1, 6, 1, 6, 1, 6, 1,
		6, 1, 6, 3, 6, 95, 8, 6, 1, 6, 1, 6, 3, 6, 99, 8, 6, 1, 6, 1, 6, 1, 6,
		1, 6, 3, 6, 105, 8, 6, 1, 6, 1, 6, 3, 6, 109, 8, 6, 1, 6, 1, 6, 1, 6, 1,
		6, 3, 6, 115, 8, 6, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 6, 123, 8, 6,
		1, 6, 4, 6, 126, 8, 6, 11, 6, 12, 6, 127, 3, 6, 130, 8, 6, 1, 6, 3, 6,
		133, 8, 6, 1, 6, 1, 6, 1, 6, 3, 6, 138, 8, 6, 1, 6, 1, 6, 1, 6, 3, 6, 143,
		8, 6, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 6, 150, 8, 6, 1, 6, 1, 6, 1, 6,
		1, 6, 3, 6, 156, 8, 6, 1, 6, 1, 6, 1, 6, 3, 6, 161, 8, 6, 1, 6, 3, 6, 164,
		8, 6, 1, 6, 1, 6, 1, 6, 3, 6, 169, 8, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 6,
		175, 8, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 6, 181, 8, 6, 1, 6, 1, 6, 1, 6, 1,
		6, 3, 6, 187, 8, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 6, 193, 8, 6, 1, 6, 1, 6,
		1, 6, 1, 6, 3, 6, 199, 8, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 6, 205, 8, 6, 1,
		6, 1, 6, 1, 6, 1, 6, 3, 6, 211, 8, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 6, 217,
		8, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 6, 223, 8, 6, 1, 6, 1, 6, 1, 6, 1, 6,
		3, 6, 229, 8, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 6, 235, 8, 6, 1, 6, 1, 6, 1,
		6, 3, 6, 240, 8, 6, 1, 6, 5, 6, 243, 8, 6, 10, 6, 12, 6, 246, 9, 6, 1,
		6, 3, 6, 249, 8, 6, 3, 6, 251, 8, 6, 1, 6, 1, 6, 1, 6, 5, 6, 256, 8, 6,
		10, 6, 12, 6, 259, 9, 6, 1, 6, 0, 1, 12, 7, 0, 2, 4, 6, 8, 10, 12, 0, 7,
		1, 0, 34, 35, 1, 0, 3, 4, 2, 0, 18, 18, 26, 27, 1, 0, 22, 24, 1, 0, 26,
		27, 1, 0, 20, 21, 1, 0, 11, 16, 324, 0, 20, 1, 0, 0, 0, 2, 23, 1, 0, 0,
		0, 4, 34, 1, 0, 0, 0, 6, 36, 1, 0, 0, 0, 8, 82, 1, 0, 0, 0, 10, 85, 1,
		0, 0, 0, 12, 163, 1, 0, 0, 0, 14, 16, 3, 2, 1, 0, 15, 14, 1, 0, 0, 0, 16,
		17, 1, 0, 0, 0, 17, 15, 1, 0, 0, 0, 17, 18, 1, 0, 0, 0, 18, 21, 1, 0, 0,
		0, 19, 21, 5, 0, 0, 1, 20, 15, 1, 0, 0, 0, 20, 19, 1, 0, 0, 0, 21, 1, 1,
		0, 0, 0, 22, 24, 5, 35, 0, 0, 23, 22, 1, 0, 0, 0, 23, 24, 1, 0, 0, 0, 24,
		25, 1, 0, 0, 0, 25, 26, 3, 8, 4, 0, 26, 27, 3, 4, 2, 0, 27, 3, 1, 0, 0,
		0, 28, 30, 7, 0, 0, 0, 29, 28, 1, 0, 0, 0, 30, 31, 1, 0, 0, 0, 31, 29,
		1, 0, 0, 0, 31, 32, 1, 0, 0, 0, 32, 35, 1, 0, 0, 0, 33, 35, 5, 0, 0, 1,
		34, 29, 1, 0, 0, 0, 34, 33, 1, 0, 0, 0, 35, 5, 1, 0, 0, 0, 36, 38, 5, 31,
		0, 0, 37, 39, 5, 35, 0, 0, 38, 37, 1, 0, 0, 0, 38, 39, 1, 0, 0, 0, 39,
		45, 1, 0, 0, 0, 40, 41, 3, 8, 4, 0, 41, 42, 3, 4, 2, 0, 42, 44, 1, 0, 0,
		0, 43, 40, 1, 0, 0, 0, 44, 47, 1, 0, 0, 0, 45, 43, 1, 0, 0, 0, 45, 46,
		1, 0, 0, 0, 46, 49, 1, 0, 0, 0, 47, 45, 1, 0, 0, 0, 48, 50, 3, 8, 4, 0,
		49, 48, 1, 0, 0, 0, 49, 50, 1, 0, 0, 0, 50, 52, 1, 0, 0, 0, 51, 53, 3,
		4, 2, 0, 52, 51, 1, 0, 0, 0, 52, 53, 1, 0, 0, 0, 53, 54, 1, 0, 0, 0, 54,
		55, 5, 32, 0, 0, 55, 7, 1, 0, 0, 0, 56, 57, 5, 2, 0, 0, 57, 59, 5, 38,
		0, 0, 58, 60, 3, 12, 6, 0, 59, 58, 1, 0, 0, 0, 59, 60, 1, 0, 0, 0, 60,
		61, 1, 0, 0, 0, 61, 62, 5, 5, 0, 0, 62, 83, 3, 12, 6, 0, 63, 64, 7, 1,
		0, 0, 64, 66, 5, 38, 0, 0, 65, 67, 3, 12, 6, 0, 66, 65, 1, 0, 0, 0, 66,
		67, 1, 0, 0, 0, 67, 68, 1, 0, 0, 0, 68, 69, 5, 5, 0, 0, 69, 83, 3, 12,
		6, 0, 70, 71, 5, 1, 0, 0, 71, 73, 5, 29, 0, 0, 72, 74, 5, 35, 0, 0, 73,
		72, 1, 0, 0, 0, 73, 74, 1, 0, 0, 0, 74, 75, 1, 0, 0, 0, 75, 77, 3, 12,
		6, 0, 76, 78, 5, 35, 0, 0, 77, 76, 1, 0, 0, 0, 77, 78, 1, 0, 0, 0, 78,
		79, 1, 0, 0, 0, 79, 80, 5, 30, 0, 0, 80, 83, 1, 0, 0, 0, 81, 83, 3, 12,
		6, 0, 82, 56, 1, 0, 0, 0, 82, 63, 1, 0, 0, 0, 82, 70, 1, 0, 0, 0, 82, 81,
		1, 0, 0, 0, 83, 9, 1, 0, 0, 0, 84, 86, 5, 3, 0, 0, 85, 84, 1, 0, 0, 0,
		85, 86, 1, 0, 0, 0, 86, 87, 1, 0, 0, 0, 87, 88, 3, 12, 6, 0, 88, 11, 1,
		0, 0, 0, 89, 90, 6, 6, -1, 0, 90, 164, 5, 37, 0, 0, 91, 164, 5, 38, 0,
		0, 92, 94, 5, 29, 0, 0, 93, 95, 5, 35, 0, 0, 94, 93, 1, 0, 0, 0, 94, 95,
		1, 0, 0, 0, 95, 96, 1, 0, 0, 0, 96, 98, 3, 12, 6, 0, 97, 99, 5, 35, 0,
		0, 98, 97, 1, 0, 0, 0, 98, 99, 1, 0, 0, 0, 99, 100, 1, 0, 0, 0, 100, 101,
		5, 30, 0, 0, 101, 164, 1, 0, 0, 0, 102, 104, 5, 17, 0, 0, 103, 105, 5,
		35, 0, 0, 104, 103, 1, 0, 0, 0, 104, 105, 1, 0, 0, 0, 105, 106, 1, 0, 0,
		0, 106, 108, 3, 12, 6, 0, 107, 109, 5, 35, 0, 0, 108, 107, 1, 0, 0, 0,
		108, 109, 1, 0, 0, 0, 109, 110, 1, 0, 0, 0, 110, 111, 5, 17, 0, 0, 111,
		164, 1, 0, 0, 0, 112, 114, 5, 29, 0, 0, 113, 115, 5, 35, 0, 0, 114, 113,
		1, 0, 0, 0, 114, 115, 1, 0, 0, 0, 115, 129, 1, 0, 0, 0, 116, 117, 3, 10,
		5, 0, 117, 118, 5, 33, 0, 0, 118, 130, 1, 0, 0, 0, 119, 125, 3, 10, 5,
		0, 120, 122, 5, 33, 0, 0, 121, 123, 5, 35, 0, 0, 122, 121, 1, 0, 0, 0,
		122, 123, 1, 0, 0, 0, 123, 124, 1, 0, 0, 0, 124, 126, 3, 10, 5, 0, 125,
		120, 1, 0, 0, 0, 126, 127, 1, 0, 0, 0, 127, 125, 1, 0, 0, 0, 127, 128,
		1, 0, 0, 0, 128, 130, 1, 0, 0, 0, 129, 116, 1, 0, 0, 0, 129, 119, 1, 0,
		0, 0, 129, 130, 1, 0, 0, 0, 130, 132, 1, 0, 0, 0, 131, 133, 5, 35, 0, 0,
		132, 131, 1, 0, 0, 0, 132, 133, 1, 0, 0, 0, 133, 134, 1, 0, 0, 0, 134,
		164, 5, 30, 0, 0, 135, 137, 5, 25, 0, 0, 136, 138, 5, 3, 0, 0, 137, 136,
		1, 0, 0, 0, 137, 138, 1, 0, 0, 0, 138, 139, 1, 0, 0, 0, 139, 164, 3, 12,
		6, 19, 140, 142, 5, 19, 0, 0, 141, 143, 5, 3, 0, 0, 142, 141, 1, 0, 0,
		0, 142, 143, 1, 0, 0, 0, 143, 144, 1, 0, 0, 0, 144, 164, 3, 12, 6, 15,
		145, 146, 7, 2, 0, 0, 146, 164, 3, 12, 6, 14, 147, 149, 5, 10, 0, 0, 148,
		150, 5, 35, 0, 0, 149, 148, 1, 0, 0, 0, 149, 150, 1, 0, 0, 0, 150, 151,
		1, 0, 0, 0, 151, 164, 3, 12, 6, 6, 152, 153, 5, 7, 0, 0, 153, 155, 3, 12,
		6, 0, 154, 156, 5, 35, 0, 0, 155, 154, 1, 0, 0, 0, 155, 156, 1, 0, 0, 0,
		156, 157, 1, 0, 0, 0, 157, 160, 3, 8, 4, 0, 158, 159, 5, 6, 0, 0, 159,
		161, 3, 8, 4, 0, 160, 158, 1, 0, 0, 0, 160, 161, 1, 0, 0, 0, 161, 164,
		1, 0, 0, 0, 162, 164, 3, 6, 3, 0, 163, 89, 1, 0, 0, 0, 163, 91, 1, 0, 0,
		0, 163, 92, 1, 0, 0, 0, 163, 102, 1, 0, 0, 0, 163, 112, 1, 0, 0, 0, 163,
		135, 1, 0, 0, 0, 163, 140, 1, 0, 0, 0, 163, 145, 1, 0, 0, 0, 163, 147,
		1, 0, 0, 0, 163, 152, 1, 0, 0, 0, 163, 162, 1, 0, 0, 0, 164, 257, 1, 0,
		0, 0, 165, 166, 10, 16, 0, 0, 166, 168, 5, 25, 0, 0, 167, 169, 5, 35, 0,
		0, 168, 167, 1, 0, 0, 0, 168, 169, 1, 0, 0, 0, 169, 170, 1, 0, 0, 0, 170,
		256, 3, 12, 6, 16, 171, 172, 10, 13, 0, 0, 172, 174, 7, 3, 0, 0, 173, 175,
		5, 35, 0, 0, 174, 173, 1, 0, 0, 0, 174, 175, 1, 0, 0, 0, 175, 176, 1, 0,
		0, 0, 176, 256, 3, 12, 6, 14, 177, 178, 10, 12, 0, 0, 178, 180, 7, 4, 0,
		0, 179, 181, 5, 35, 0, 0, 180, 179, 1, 0, 0, 0, 180, 181, 1, 0, 0, 0, 181,
		182, 1, 0, 0, 0, 182, 256, 3, 12, 6, 13, 183, 184, 10, 11, 0, 0, 184, 186,
		7, 5, 0, 0, 185, 187, 5, 35, 0, 0, 186, 185, 1, 0, 0, 0, 186, 187, 1, 0,
		0, 0, 187, 188, 1, 0, 0, 0, 188, 256, 3, 12, 6, 12, 189, 190, 10, 10, 0,
		0, 190, 192, 5, 19, 0, 0, 191, 193, 5, 35, 0, 0, 192, 191, 1, 0, 0, 0,
		192, 193, 1, 0, 0, 0, 193, 194, 1, 0, 0, 0, 194, 256, 3, 12, 6, 11, 195,
		196, 10, 9, 0, 0, 196, 198, 5, 18, 0, 0, 197, 199, 5, 35, 0, 0, 198, 197,
		1, 0, 0, 0, 198, 199, 1, 0, 0, 0, 199, 200, 1, 0, 0, 0, 200, 256, 3, 12,
		6, 10, 201, 202, 10, 8, 0, 0, 202, 204, 5, 17, 0, 0, 203, 205, 5, 35, 0,
		0, 204, 203, 1, 0, 0, 0, 204, 205, 1, 0, 0, 0, 205, 206, 1, 0, 0, 0, 206,
		256, 3, 12, 6, 9, 207, 208, 10, 7, 0, 0, 208, 210, 7, 6, 0, 0, 209, 211,
		5, 35, 0, 0, 210, 209, 1, 0, 0, 0, 210, 211, 1, 0, 0, 0, 211, 212, 1, 0,
		0, 0, 212, 256, 3, 12, 6, 8, 213, 214, 10, 5, 0, 0, 214, 216, 5, 9, 0,
		0, 215, 217, 5, 35, 0, 0, 216, 215, 1, 0, 0, 0, 216, 217, 1, 0, 0, 0, 217,
		218, 1, 0, 0, 0, 218, 256, 3, 12, 6, 6, 219, 220, 10, 4, 0, 0, 220, 222,
		5, 8, 0, 0, 221, 223, 5, 35, 0, 0, 222, 221, 1, 0, 0, 0, 222, 223, 1, 0,
		0, 0, 223, 224, 1, 0, 0, 0, 224, 256, 3, 12, 6, 5, 225, 226, 10, 3, 0,
		0, 226, 228, 5, 5, 0, 0, 227, 229, 5, 35, 0, 0, 228, 227, 1, 0, 0, 0, 228,
		229, 1, 0, 0, 0, 229, 230, 1, 0, 0, 0, 230, 256, 3, 12, 6, 4, 231, 232,
		10, 18, 0, 0, 232, 234, 5, 29, 0, 0, 233, 235, 5, 35, 0, 0, 234, 233, 1,
		0, 0, 0, 234, 235, 1, 0, 0, 0, 235, 250, 1, 0, 0, 0, 236, 244, 3, 12, 6,
		0, 237, 239, 5, 33, 0, 0, 238, 240, 5, 35, 0, 0, 239, 238, 1, 0, 0, 0,
		239, 240, 1, 0, 0, 0, 240, 241, 1, 0, 0, 0, 241, 243, 3, 12, 6, 0, 242,
		237, 1, 0, 0, 0, 243, 246, 1, 0, 0, 0, 244, 242, 1, 0, 0, 0, 244, 245,
		1, 0, 0, 0, 245, 248, 1, 0, 0, 0, 246, 244, 1, 0, 0, 0, 247, 249, 5, 35,
		0, 0, 248, 247, 1, 0, 0, 0, 248, 249, 1, 0, 0, 0, 249, 251, 1, 0, 0, 0,
		250, 236, 1, 0, 0, 0, 250, 251, 1, 0, 0, 0, 251, 252, 1, 0, 0, 0, 252,
		256, 5, 30, 0, 0, 253, 254, 10, 17, 0, 0, 254, 256, 5, 25, 0, 0, 255, 165,
		1, 0, 0, 0, 255, 171, 1, 0, 0, 0, 255, 177, 1, 0, 0, 0, 255, 183, 1, 0,
		0, 0, 255, 189, 1, 0, 0, 0, 255, 195, 1, 0, 0, 0, 255, 201, 1, 0, 0, 0,
		255, 207, 1, 0, 0, 0, 255, 213, 1, 0, 0, 0, 255, 219, 1, 0, 0, 0, 255,
		225, 1, 0, 0, 0, 255, 231, 1, 0, 0, 0, 255, 253, 1, 0, 0, 0, 256, 259,
		1, 0, 0, 0, 257, 255, 1, 0, 0, 0, 257, 258, 1, 0, 0, 0, 258, 13, 1, 0,
		0, 0, 259, 257, 1, 0, 0, 0, 48, 17, 20, 23, 31, 34, 38, 45, 49, 52, 59,
		66, 73, 77, 82, 85, 94, 98, 104, 108, 114, 122, 127, 129, 132, 137, 142,
		149, 155, 160, 163, 168, 174, 180, 186, 192, 198, 204, 210, 216, 222, 228,
		234, 239, 244, 248, 250, 255, 257,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// LynxParserInit initializes any static state used to implement LynxParser. By default the
// static state used to implement the parser is lazily initialized during the first call to
// NewLynxParser(). You can call this function if you wish to initialize the static state ahead
// of time.
func LynxParserInit() {
	staticData := &LynxParserParserStaticData
	staticData.once.Do(lynxparserParserInit)
}

// NewLynxParser produces a new parser instance for the optional input antlr.TokenStream.
func NewLynxParser(input antlr.TokenStream) *LynxParser {
	LynxParserInit()
	this := new(LynxParser)
	this.BaseParser = antlr.NewBaseParser(input)
	staticData := &LynxParserParserStaticData
	this.Interpreter = antlr.NewParserATNSimulator(this, staticData.atn, staticData.decisionToDFA, staticData.PredictionContextCache)
	this.RuleNames = staticData.RuleNames
	this.LiteralNames = staticData.LiteralNames
	this.SymbolicNames = staticData.SymbolicNames
	this.GrammarFileName = "LynxParser.g4"

	return this
}

// LynxParser tokens.
const (
	LynxParserEOF       = antlr.TokenEOF
	LynxParserPRINT     = 1
	LynxParserCONST     = 2
	LynxParserVAR       = 3
	LynxParserVAL       = 4
	LynxParserASSIGN    = 5
	LynxParserELSE      = 6
	LynxParserIF        = 7
	LynxParserOR        = 8
	LynxParserAND       = 9
	LynxParserNOT       = 10
	LynxParserLT        = 11
	LynxParserGT        = 12
	LynxParserLTE       = 13
	LynxParserGTE       = 14
	LynxParserEQ        = 15
	LynxParserNE        = 16
	LynxParserBITOR     = 17
	LynxParserBITXOR    = 18
	LynxParserBITAND    = 19
	LynxParserLSHIFT    = 20
	LynxParserRSHIFT    = 21
	LynxParserPERCENT   = 22
	LynxParserSLASH     = 23
	LynxParserSTAR      = 24
	LynxParserCARET     = 25
	LynxParserPLUS      = 26
	LynxParserMINUS     = 27
	LynxParserDOT       = 28
	LynxParserLPAREN    = 29
	LynxParserRPAREN    = 30
	LynxParserLBRACE    = 31
	LynxParserRBRACE    = 32
	LynxParserCOMMA     = 33
	LynxParserSEMICOLON = 34
	LynxParserNL        = 35
	LynxParserIGNORED   = 36
	LynxParserNUM       = 37
	LynxParserID        = 38
)

// LynxParser rules.
const (
	LynxParserRULE_fileStat  = 0
	LynxParserRULE_endedStat = 1
	LynxParserRULE_lineEnd   = 2
	LynxParserRULE_multiStat = 3
	LynxParserRULE_stat      = 4
	LynxParserRULE_tupleElem = 5
	LynxParserRULE_expr      = 6
)

// IFileStatContext is an interface to support dynamic dispatch.
type IFileStatContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllEndedStat() []IEndedStatContext
	EndedStat(i int) IEndedStatContext
	EOF() antlr.TerminalNode

	// IsFileStatContext differentiates from other interfaces.
	IsFileStatContext()
}

type FileStatContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFileStatContext() *FileStatContext {
	var p = new(FileStatContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_fileStat
	return p
}

func InitEmptyFileStatContext(p *FileStatContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_fileStat
}

func (*FileStatContext) IsFileStatContext() {}

func NewFileStatContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FileStatContext {
	var p = new(FileStatContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = LynxParserRULE_fileStat

	return p
}

func (s *FileStatContext) GetParser() antlr.Parser { return s.parser }

func (s *FileStatContext) AllEndedStat() []IEndedStatContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IEndedStatContext); ok {
			len++
		}
	}

	tst := make([]IEndedStatContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IEndedStatContext); ok {
			tst[i] = t.(IEndedStatContext)
			i++
		}
	}

	return tst
}

func (s *FileStatContext) EndedStat(i int) IEndedStatContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IEndedStatContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IEndedStatContext)
}

func (s *FileStatContext) EOF() antlr.TerminalNode {
	return s.GetToken(LynxParserEOF, 0)
}

func (s *FileStatContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FileStatContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FileStatContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterFileStat(s)
	}
}

func (s *FileStatContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitFileStat(s)
	}
}

func (s *FileStatContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitFileStat(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *LynxParser) FileStat() (localctx IFileStatContext) {
	localctx = NewFileStatContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, LynxParserRULE_fileStat)
	var _la int

	p.SetState(20)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case LynxParserPRINT, LynxParserCONST, LynxParserVAR, LynxParserVAL, LynxParserIF, LynxParserNOT, LynxParserBITOR, LynxParserBITXOR, LynxParserBITAND, LynxParserCARET, LynxParserPLUS, LynxParserMINUS, LynxParserLPAREN, LynxParserLBRACE, LynxParserNL, LynxParserNUM, LynxParserID:
		p.EnterOuterAlt(localctx, 1)
		p.SetState(15)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		for ok := true; ok; ok = ((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&449596753054) != 0) {
			{
				p.SetState(14)
				p.EndedStat()
			}

			p.SetState(17)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}
			_la = p.GetTokenStream().LA(1)
		}

	case LynxParserEOF:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(19)
			p.Match(LynxParserEOF)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IEndedStatContext is an interface to support dynamic dispatch.
type IEndedStatContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Stat() IStatContext
	LineEnd() ILineEndContext
	NL() antlr.TerminalNode

	// IsEndedStatContext differentiates from other interfaces.
	IsEndedStatContext()
}

type EndedStatContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEndedStatContext() *EndedStatContext {
	var p = new(EndedStatContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_endedStat
	return p
}

func InitEmptyEndedStatContext(p *EndedStatContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_endedStat
}

func (*EndedStatContext) IsEndedStatContext() {}

func NewEndedStatContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EndedStatContext {
	var p = new(EndedStatContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = LynxParserRULE_endedStat

	return p
}

func (s *EndedStatContext) GetParser() antlr.Parser { return s.parser }

func (s *EndedStatContext) Stat() IStatContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IStatContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IStatContext)
}

func (s *EndedStatContext) LineEnd() ILineEndContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ILineEndContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ILineEndContext)
}

func (s *EndedStatContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *EndedStatContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EndedStatContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EndedStatContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterEndedStat(s)
	}
}

func (s *EndedStatContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitEndedStat(s)
	}
}

func (s *EndedStatContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitEndedStat(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *LynxParser) EndedStat() (localctx IEndedStatContext) {
	localctx = NewEndedStatContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, LynxParserRULE_endedStat)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(23)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == LynxParserNL {
		{
			p.SetState(22)
			p.Match(LynxParserNL)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}
	{
		p.SetState(25)
		p.Stat()
	}
	{
		p.SetState(26)
		p.LineEnd()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ILineEndContext is an interface to support dynamic dispatch.
type ILineEndContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllNL() []antlr.TerminalNode
	NL(i int) antlr.TerminalNode
	AllSEMICOLON() []antlr.TerminalNode
	SEMICOLON(i int) antlr.TerminalNode
	EOF() antlr.TerminalNode

	// IsLineEndContext differentiates from other interfaces.
	IsLineEndContext()
}

type LineEndContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyLineEndContext() *LineEndContext {
	var p = new(LineEndContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_lineEnd
	return p
}

func InitEmptyLineEndContext(p *LineEndContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_lineEnd
}

func (*LineEndContext) IsLineEndContext() {}

func NewLineEndContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *LineEndContext {
	var p = new(LineEndContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = LynxParserRULE_lineEnd

	return p
}

func (s *LineEndContext) GetParser() antlr.Parser { return s.parser }

func (s *LineEndContext) AllNL() []antlr.TerminalNode {
	return s.GetTokens(LynxParserNL)
}

func (s *LineEndContext) NL(i int) antlr.TerminalNode {
	return s.GetToken(LynxParserNL, i)
}

func (s *LineEndContext) AllSEMICOLON() []antlr.TerminalNode {
	return s.GetTokens(LynxParserSEMICOLON)
}

func (s *LineEndContext) SEMICOLON(i int) antlr.TerminalNode {
	return s.GetToken(LynxParserSEMICOLON, i)
}

func (s *LineEndContext) EOF() antlr.TerminalNode {
	return s.GetToken(LynxParserEOF, 0)
}

func (s *LineEndContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LineEndContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *LineEndContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterLineEnd(s)
	}
}

func (s *LineEndContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitLineEnd(s)
	}
}

func (s *LineEndContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitLineEnd(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *LynxParser) LineEnd() (localctx ILineEndContext) {
	localctx = NewLineEndContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, LynxParserRULE_lineEnd)
	var _la int

	var _alt int

	p.SetState(34)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case LynxParserSEMICOLON, LynxParserNL:
		p.EnterOuterAlt(localctx, 1)
		p.SetState(29)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_alt = 1
		for ok := true; ok; ok = _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
			switch _alt {
			case 1:
				{
					p.SetState(28)
					_la = p.GetTokenStream().LA(1)

					if !(_la == LynxParserSEMICOLON || _la == LynxParserNL) {
						p.GetErrorHandler().RecoverInline(p)
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}

			default:
				p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
				goto errorExit
			}

			p.SetState(31)
			p.GetErrorHandler().Sync(p)
			_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 3, p.GetParserRuleContext())
			if p.HasError() {
				goto errorExit
			}
		}

	case LynxParserEOF:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(33)
			p.Match(LynxParserEOF)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IMultiStatContext is an interface to support dynamic dispatch.
type IMultiStatContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	LBRACE() antlr.TerminalNode
	RBRACE() antlr.TerminalNode
	NL() antlr.TerminalNode
	AllStat() []IStatContext
	Stat(i int) IStatContext
	AllLineEnd() []ILineEndContext
	LineEnd(i int) ILineEndContext

	// IsMultiStatContext differentiates from other interfaces.
	IsMultiStatContext()
}

type MultiStatContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMultiStatContext() *MultiStatContext {
	var p = new(MultiStatContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_multiStat
	return p
}

func InitEmptyMultiStatContext(p *MultiStatContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_multiStat
}

func (*MultiStatContext) IsMultiStatContext() {}

func NewMultiStatContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MultiStatContext {
	var p = new(MultiStatContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = LynxParserRULE_multiStat

	return p
}

func (s *MultiStatContext) GetParser() antlr.Parser { return s.parser }

func (s *MultiStatContext) LBRACE() antlr.TerminalNode {
	return s.GetToken(LynxParserLBRACE, 0)
}

func (s *MultiStatContext) RBRACE() antlr.TerminalNode {
	return s.GetToken(LynxParserRBRACE, 0)
}

func (s *MultiStatContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *MultiStatContext) AllStat() []IStatContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IStatContext); ok {
			len++
		}
	}

	tst := make([]IStatContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IStatContext); ok {
			tst[i] = t.(IStatContext)
			i++
		}
	}

	return tst
}

func (s *MultiStatContext) Stat(i int) IStatContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IStatContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IStatContext)
}

func (s *MultiStatContext) AllLineEnd() []ILineEndContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(ILineEndContext); ok {
			len++
		}
	}

	tst := make([]ILineEndContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(ILineEndContext); ok {
			tst[i] = t.(ILineEndContext)
			i++
		}
	}

	return tst
}

func (s *MultiStatContext) LineEnd(i int) ILineEndContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ILineEndContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(ILineEndContext)
}

func (s *MultiStatContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MultiStatContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MultiStatContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterMultiStat(s)
	}
}

func (s *MultiStatContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitMultiStat(s)
	}
}

func (s *MultiStatContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitMultiStat(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *LynxParser) MultiStat() (localctx IMultiStatContext) {
	localctx = NewMultiStatContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, LynxParserRULE_multiStat)
	var _la int

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(36)
		p.Match(LynxParserLBRACE)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(38)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 5, p.GetParserRuleContext()) == 1 {
		{
			p.SetState(37)
			p.Match(LynxParserNL)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	} else if p.HasError() { // JIM
		goto errorExit
	}
	p.SetState(45)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 6, p.GetParserRuleContext())
	if p.HasError() {
		goto errorExit
	}
	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			{
				p.SetState(40)
				p.Stat()
			}
			{
				p.SetState(41)
				p.LineEnd()
			}

		}
		p.SetState(47)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 6, p.GetParserRuleContext())
		if p.HasError() {
			goto errorExit
		}
	}
	p.SetState(49)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&415237014686) != 0 {
		{
			p.SetState(48)
			p.Stat()
		}

	}
	p.SetState(52)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if (int64((_la - -1)) & ^0x3f) == 0 && ((int64(1)<<(_la - -1))&103079215105) != 0 {
		{
			p.SetState(51)
			p.LineEnd()
		}

	}
	{
		p.SetState(54)
		p.Match(LynxParserRBRACE)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IStatContext is an interface to support dynamic dispatch.
type IStatContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsStatContext differentiates from other interfaces.
	IsStatContext()
}

type StatContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStatContext() *StatContext {
	var p = new(StatContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_stat
	return p
}

func InitEmptyStatContext(p *StatContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_stat
}

func (*StatContext) IsStatContext() {}

func NewStatContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StatContext {
	var p = new(StatContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = LynxParserRULE_stat

	return p
}

func (s *StatContext) GetParser() antlr.Parser { return s.parser }

func (s *StatContext) CopyAll(ctx *StatContext) {
	s.CopyFrom(&ctx.BaseParserRuleContext)
}

func (s *StatContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StatContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type PrintStatContext struct {
	StatContext
}

func NewPrintStatContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *PrintStatContext {
	var p = new(PrintStatContext)

	InitEmptyStatContext(&p.StatContext)
	p.parser = parser
	p.CopyAll(ctx.(*StatContext))

	return p
}

func (s *PrintStatContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PrintStatContext) PRINT() antlr.TerminalNode {
	return s.GetToken(LynxParserPRINT, 0)
}

func (s *PrintStatContext) LPAREN() antlr.TerminalNode {
	return s.GetToken(LynxParserLPAREN, 0)
}

func (s *PrintStatContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *PrintStatContext) RPAREN() antlr.TerminalNode {
	return s.GetToken(LynxParserRPAREN, 0)
}

func (s *PrintStatContext) AllNL() []antlr.TerminalNode {
	return s.GetTokens(LynxParserNL)
}

func (s *PrintStatContext) NL(i int) antlr.TerminalNode {
	return s.GetToken(LynxParserNL, i)
}

func (s *PrintStatContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterPrintStat(s)
	}
}

func (s *PrintStatContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitPrintStat(s)
	}
}

func (s *PrintStatContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitPrintStat(s)

	default:
		return t.VisitChildren(s)
	}
}

type ExprStatContext struct {
	StatContext
}

func NewExprStatContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ExprStatContext {
	var p = new(ExprStatContext)

	InitEmptyStatContext(&p.StatContext)
	p.parser = parser
	p.CopyAll(ctx.(*StatContext))

	return p
}

func (s *ExprStatContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExprStatContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *ExprStatContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterExprStat(s)
	}
}

func (s *ExprStatContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitExprStat(s)
	}
}

func (s *ExprStatContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitExprStat(s)

	default:
		return t.VisitChildren(s)
	}
}

type ConstStatContext struct {
	StatContext
	Hint  IExprContext
	Right IExprContext
}

func NewConstStatContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ConstStatContext {
	var p = new(ConstStatContext)

	InitEmptyStatContext(&p.StatContext)
	p.parser = parser
	p.CopyAll(ctx.(*StatContext))

	return p
}

func (s *ConstStatContext) GetHint() IExprContext { return s.Hint }

func (s *ConstStatContext) GetRight() IExprContext { return s.Right }

func (s *ConstStatContext) SetHint(v IExprContext) { s.Hint = v }

func (s *ConstStatContext) SetRight(v IExprContext) { s.Right = v }

func (s *ConstStatContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ConstStatContext) CONST() antlr.TerminalNode {
	return s.GetToken(LynxParserCONST, 0)
}

func (s *ConstStatContext) ID() antlr.TerminalNode {
	return s.GetToken(LynxParserID, 0)
}

func (s *ConstStatContext) ASSIGN() antlr.TerminalNode {
	return s.GetToken(LynxParserASSIGN, 0)
}

func (s *ConstStatContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *ConstStatContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *ConstStatContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterConstStat(s)
	}
}

func (s *ConstStatContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitConstStat(s)
	}
}

func (s *ConstStatContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitConstStat(s)

	default:
		return t.VisitChildren(s)
	}
}

type DeclStatContext struct {
	StatContext
	Key   antlr.Token
	Hint  IExprContext
	Right IExprContext
}

func NewDeclStatContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *DeclStatContext {
	var p = new(DeclStatContext)

	InitEmptyStatContext(&p.StatContext)
	p.parser = parser
	p.CopyAll(ctx.(*StatContext))

	return p
}

func (s *DeclStatContext) GetKey() antlr.Token { return s.Key }

func (s *DeclStatContext) SetKey(v antlr.Token) { s.Key = v }

func (s *DeclStatContext) GetHint() IExprContext { return s.Hint }

func (s *DeclStatContext) GetRight() IExprContext { return s.Right }

func (s *DeclStatContext) SetHint(v IExprContext) { s.Hint = v }

func (s *DeclStatContext) SetRight(v IExprContext) { s.Right = v }

func (s *DeclStatContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DeclStatContext) ID() antlr.TerminalNode {
	return s.GetToken(LynxParserID, 0)
}

func (s *DeclStatContext) ASSIGN() antlr.TerminalNode {
	return s.GetToken(LynxParserASSIGN, 0)
}

func (s *DeclStatContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *DeclStatContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *DeclStatContext) VAL() antlr.TerminalNode {
	return s.GetToken(LynxParserVAL, 0)
}

func (s *DeclStatContext) VAR() antlr.TerminalNode {
	return s.GetToken(LynxParserVAR, 0)
}

func (s *DeclStatContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterDeclStat(s)
	}
}

func (s *DeclStatContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitDeclStat(s)
	}
}

func (s *DeclStatContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitDeclStat(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *LynxParser) Stat() (localctx IStatContext) {
	localctx = NewStatContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, LynxParserRULE_stat)
	var _la int

	p.SetState(82)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case LynxParserCONST:
		localctx = NewConstStatContext(p, localctx)
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(56)
			p.Match(LynxParserCONST)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(57)
			p.Match(LynxParserID)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(59)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&415237014656) != 0 {
			{
				p.SetState(58)

				var _x = p.expr(0)

				localctx.(*ConstStatContext).Hint = _x
			}

		}
		{
			p.SetState(61)
			p.Match(LynxParserASSIGN)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(62)

			var _x = p.expr(0)

			localctx.(*ConstStatContext).Right = _x
		}

	case LynxParserVAR, LynxParserVAL:
		localctx = NewDeclStatContext(p, localctx)
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(63)

			var _lt = p.GetTokenStream().LT(1)

			localctx.(*DeclStatContext).Key = _lt

			_la = p.GetTokenStream().LA(1)

			if !(_la == LynxParserVAR || _la == LynxParserVAL) {
				var _ri = p.GetErrorHandler().RecoverInline(p)

				localctx.(*DeclStatContext).Key = _ri
			} else {
				p.GetErrorHandler().ReportMatch(p)
				p.Consume()
			}
		}
		{
			p.SetState(64)
			p.Match(LynxParserID)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(66)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&415237014656) != 0 {
			{
				p.SetState(65)

				var _x = p.expr(0)

				localctx.(*DeclStatContext).Hint = _x
			}

		}
		{
			p.SetState(68)
			p.Match(LynxParserASSIGN)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(69)

			var _x = p.expr(0)

			localctx.(*DeclStatContext).Right = _x
		}

	case LynxParserPRINT:
		localctx = NewPrintStatContext(p, localctx)
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(70)
			p.Match(LynxParserPRINT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(71)
			p.Match(LynxParserLPAREN)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(73)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserNL {
			{
				p.SetState(72)
				p.Match(LynxParserNL)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(75)
			p.expr(0)
		}
		p.SetState(77)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserNL {
			{
				p.SetState(76)
				p.Match(LynxParserNL)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(79)
			p.Match(LynxParserRPAREN)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case LynxParserIF, LynxParserNOT, LynxParserBITOR, LynxParserBITXOR, LynxParserBITAND, LynxParserCARET, LynxParserPLUS, LynxParserMINUS, LynxParserLPAREN, LynxParserLBRACE, LynxParserNUM, LynxParserID:
		localctx = NewExprStatContext(p, localctx)
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(81)
			p.expr(0)
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ITupleElemContext is an interface to support dynamic dispatch.
type ITupleElemContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Expr() IExprContext
	VAR() antlr.TerminalNode

	// IsTupleElemContext differentiates from other interfaces.
	IsTupleElemContext()
}

type TupleElemContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTupleElemContext() *TupleElemContext {
	var p = new(TupleElemContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_tupleElem
	return p
}

func InitEmptyTupleElemContext(p *TupleElemContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_tupleElem
}

func (*TupleElemContext) IsTupleElemContext() {}

func NewTupleElemContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TupleElemContext {
	var p = new(TupleElemContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = LynxParserRULE_tupleElem

	return p
}

func (s *TupleElemContext) GetParser() antlr.Parser { return s.parser }

func (s *TupleElemContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *TupleElemContext) VAR() antlr.TerminalNode {
	return s.GetToken(LynxParserVAR, 0)
}

func (s *TupleElemContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TupleElemContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TupleElemContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterTupleElem(s)
	}
}

func (s *TupleElemContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitTupleElem(s)
	}
}

func (s *TupleElemContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitTupleElem(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *LynxParser) TupleElem() (localctx ITupleElemContext) {
	localctx = NewTupleElemContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, LynxParserRULE_tupleElem)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(85)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == LynxParserVAR {
		{
			p.SetState(84)
			p.Match(LynxParserVAR)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}
	{
		p.SetState(87)
		p.expr(0)
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IExprContext is an interface to support dynamic dispatch.
type IExprContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsExprContext differentiates from other interfaces.
	IsExprContext()
}

type ExprContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyExprContext() *ExprContext {
	var p = new(ExprContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_expr
	return p
}

func InitEmptyExprContext(p *ExprContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = LynxParserRULE_expr
}

func (*ExprContext) IsExprContext() {}

func NewExprContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExprContext {
	var p = new(ExprContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = LynxParserRULE_expr

	return p
}

func (s *ExprContext) GetParser() antlr.Parser { return s.parser }

func (s *ExprContext) CopyAll(ctx *ExprContext) {
	s.CopyFrom(&ctx.BaseParserRuleContext)
}

func (s *ExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExprContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type MultiStat_Context struct {
	ExprContext
}

func NewMultiStat_Context(parser antlr.Parser, ctx antlr.ParserRuleContext) *MultiStat_Context {
	var p = new(MultiStat_Context)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *MultiStat_Context) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MultiStat_Context) MultiStat() IMultiStatContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IMultiStatContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IMultiStatContext)
}

func (s *MultiStat_Context) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterMultiStat_(s)
	}
}

func (s *MultiStat_Context) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitMultiStat_(s)
	}
}

func (s *MultiStat_Context) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitMultiStat_(s)

	default:
		return t.VisitChildren(s)
	}
}

type OrExprContext struct {
	ExprContext
	Left  IExprContext
	Right IExprContext
}

func NewOrExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *OrExprContext {
	var p = new(OrExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *OrExprContext) GetLeft() IExprContext { return s.Left }

func (s *OrExprContext) GetRight() IExprContext { return s.Right }

func (s *OrExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *OrExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *OrExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OrExprContext) OR() antlr.TerminalNode {
	return s.GetToken(LynxParserOR, 0)
}

func (s *OrExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *OrExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *OrExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *OrExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterOrExpr(s)
	}
}

func (s *OrExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitOrExpr(s)
	}
}

func (s *OrExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitOrExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type AddrExprContext struct {
	ExprContext
}

func NewAddrExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *AddrExprContext {
	var p = new(AddrExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *AddrExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AddrExprContext) BITAND() antlr.TerminalNode {
	return s.GetToken(LynxParserBITAND, 0)
}

func (s *AddrExprContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *AddrExprContext) VAR() antlr.TerminalNode {
	return s.GetToken(LynxParserVAR, 0)
}

func (s *AddrExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterAddrExpr(s)
	}
}

func (s *AddrExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitAddrExpr(s)
	}
}

func (s *AddrExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitAddrExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type NumExprContext struct {
	ExprContext
}

func NewNumExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *NumExprContext {
	var p = new(NumExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *NumExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NumExprContext) NUM() antlr.TerminalNode {
	return s.GetToken(LynxParserNUM, 0)
}

func (s *NumExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterNumExpr(s)
	}
}

func (s *NumExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitNumExpr(s)
	}
}

func (s *NumExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitNumExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type ParenExprContext struct {
	ExprContext
}

func NewParenExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ParenExprContext {
	var p = new(ParenExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *ParenExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ParenExprContext) LPAREN() antlr.TerminalNode {
	return s.GetToken(LynxParserLPAREN, 0)
}

func (s *ParenExprContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *ParenExprContext) RPAREN() antlr.TerminalNode {
	return s.GetToken(LynxParserRPAREN, 0)
}

func (s *ParenExprContext) AllNL() []antlr.TerminalNode {
	return s.GetTokens(LynxParserNL)
}

func (s *ParenExprContext) NL(i int) antlr.TerminalNode {
	return s.GetToken(LynxParserNL, i)
}

func (s *ParenExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterParenExpr(s)
	}
}

func (s *ParenExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitParenExpr(s)
	}
}

func (s *ParenExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitParenExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type BitXorExprContext struct {
	ExprContext
	Left  IExprContext
	Right IExprContext
}

func NewBitXorExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BitXorExprContext {
	var p = new(BitXorExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *BitXorExprContext) GetLeft() IExprContext { return s.Left }

func (s *BitXorExprContext) GetRight() IExprContext { return s.Right }

func (s *BitXorExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *BitXorExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *BitXorExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BitXorExprContext) BITXOR() antlr.TerminalNode {
	return s.GetToken(LynxParserBITXOR, 0)
}

func (s *BitXorExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *BitXorExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *BitXorExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *BitXorExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterBitXorExpr(s)
	}
}

func (s *BitXorExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitBitXorExpr(s)
	}
}

func (s *BitXorExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitBitXorExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type ShiftExprContext struct {
	ExprContext
	Left  IExprContext
	Op    antlr.Token
	Right IExprContext
}

func NewShiftExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ShiftExprContext {
	var p = new(ShiftExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *ShiftExprContext) GetOp() antlr.Token { return s.Op }

func (s *ShiftExprContext) SetOp(v antlr.Token) { s.Op = v }

func (s *ShiftExprContext) GetLeft() IExprContext { return s.Left }

func (s *ShiftExprContext) GetRight() IExprContext { return s.Right }

func (s *ShiftExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *ShiftExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *ShiftExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ShiftExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *ShiftExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *ShiftExprContext) LSHIFT() antlr.TerminalNode {
	return s.GetToken(LynxParserLSHIFT, 0)
}

func (s *ShiftExprContext) RSHIFT() antlr.TerminalNode {
	return s.GetToken(LynxParserRSHIFT, 0)
}

func (s *ShiftExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *ShiftExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterShiftExpr(s)
	}
}

func (s *ShiftExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitShiftExpr(s)
	}
}

func (s *ShiftExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitShiftExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type MagExprContext struct {
	ExprContext
}

func NewMagExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *MagExprContext {
	var p = new(MagExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *MagExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MagExprContext) AllBITOR() []antlr.TerminalNode {
	return s.GetTokens(LynxParserBITOR)
}

func (s *MagExprContext) BITOR(i int) antlr.TerminalNode {
	return s.GetToken(LynxParserBITOR, i)
}

func (s *MagExprContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *MagExprContext) AllNL() []antlr.TerminalNode {
	return s.GetTokens(LynxParserNL)
}

func (s *MagExprContext) NL(i int) antlr.TerminalNode {
	return s.GetToken(LynxParserNL, i)
}

func (s *MagExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterMagExpr(s)
	}
}

func (s *MagExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitMagExpr(s)
	}
}

func (s *MagExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitMagExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type PrefixExprContext struct {
	ExprContext
	Op antlr.Token
}

func NewPrefixExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *PrefixExprContext {
	var p = new(PrefixExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *PrefixExprContext) GetOp() antlr.Token { return s.Op }

func (s *PrefixExprContext) SetOp(v antlr.Token) { s.Op = v }

func (s *PrefixExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PrefixExprContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *PrefixExprContext) PLUS() antlr.TerminalNode {
	return s.GetToken(LynxParserPLUS, 0)
}

func (s *PrefixExprContext) MINUS() antlr.TerminalNode {
	return s.GetToken(LynxParserMINUS, 0)
}

func (s *PrefixExprContext) BITXOR() antlr.TerminalNode {
	return s.GetToken(LynxParserBITXOR, 0)
}

func (s *PrefixExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterPrefixExpr(s)
	}
}

func (s *PrefixExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitPrefixExpr(s)
	}
}

func (s *PrefixExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitPrefixExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type BitOrExprContext struct {
	ExprContext
	Left  IExprContext
	Right IExprContext
}

func NewBitOrExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BitOrExprContext {
	var p = new(BitOrExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *BitOrExprContext) GetLeft() IExprContext { return s.Left }

func (s *BitOrExprContext) GetRight() IExprContext { return s.Right }

func (s *BitOrExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *BitOrExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *BitOrExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BitOrExprContext) BITOR() antlr.TerminalNode {
	return s.GetToken(LynxParserBITOR, 0)
}

func (s *BitOrExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *BitOrExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *BitOrExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *BitOrExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterBitOrExpr(s)
	}
}

func (s *BitOrExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitBitOrExpr(s)
	}
}

func (s *BitOrExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitBitOrExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type NotExprContext struct {
	ExprContext
}

func NewNotExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *NotExprContext {
	var p = new(NotExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *NotExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NotExprContext) NOT() antlr.TerminalNode {
	return s.GetToken(LynxParserNOT, 0)
}

func (s *NotExprContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *NotExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *NotExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterNotExpr(s)
	}
}

func (s *NotExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitNotExpr(s)
	}
}

func (s *NotExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitNotExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type AddExprContext struct {
	ExprContext
	Left  IExprContext
	Op    antlr.Token
	Right IExprContext
}

func NewAddExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *AddExprContext {
	var p = new(AddExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *AddExprContext) GetOp() antlr.Token { return s.Op }

func (s *AddExprContext) SetOp(v antlr.Token) { s.Op = v }

func (s *AddExprContext) GetLeft() IExprContext { return s.Left }

func (s *AddExprContext) GetRight() IExprContext { return s.Right }

func (s *AddExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *AddExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *AddExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AddExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *AddExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *AddExprContext) PLUS() antlr.TerminalNode {
	return s.GetToken(LynxParserPLUS, 0)
}

func (s *AddExprContext) MINUS() antlr.TerminalNode {
	return s.GetToken(LynxParserMINUS, 0)
}

func (s *AddExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *AddExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterAddExpr(s)
	}
}

func (s *AddExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitAddExpr(s)
	}
}

func (s *AddExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitAddExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type CompExprContext struct {
	ExprContext
	Left  IExprContext
	Op    antlr.Token
	Right IExprContext
}

func NewCompExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *CompExprContext {
	var p = new(CompExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *CompExprContext) GetOp() antlr.Token { return s.Op }

func (s *CompExprContext) SetOp(v antlr.Token) { s.Op = v }

func (s *CompExprContext) GetLeft() IExprContext { return s.Left }

func (s *CompExprContext) GetRight() IExprContext { return s.Right }

func (s *CompExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *CompExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *CompExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CompExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *CompExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *CompExprContext) LT() antlr.TerminalNode {
	return s.GetToken(LynxParserLT, 0)
}

func (s *CompExprContext) GT() antlr.TerminalNode {
	return s.GetToken(LynxParserGT, 0)
}

func (s *CompExprContext) LTE() antlr.TerminalNode {
	return s.GetToken(LynxParserLTE, 0)
}

func (s *CompExprContext) GTE() antlr.TerminalNode {
	return s.GetToken(LynxParserGTE, 0)
}

func (s *CompExprContext) EQ() antlr.TerminalNode {
	return s.GetToken(LynxParserEQ, 0)
}

func (s *CompExprContext) NE() antlr.TerminalNode {
	return s.GetToken(LynxParserNE, 0)
}

func (s *CompExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *CompExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterCompExpr(s)
	}
}

func (s *CompExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitCompExpr(s)
	}
}

func (s *CompExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitCompExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type DerefExprContext struct {
	ExprContext
}

func NewDerefExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *DerefExprContext {
	var p = new(DerefExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *DerefExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DerefExprContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *DerefExprContext) CARET() antlr.TerminalNode {
	return s.GetToken(LynxParserCARET, 0)
}

func (s *DerefExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterDerefExpr(s)
	}
}

func (s *DerefExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitDerefExpr(s)
	}
}

func (s *DerefExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitDerefExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type MulExprContext struct {
	ExprContext
	Left  IExprContext
	Op    antlr.Token
	Right IExprContext
}

func NewMulExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *MulExprContext {
	var p = new(MulExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *MulExprContext) GetOp() antlr.Token { return s.Op }

func (s *MulExprContext) SetOp(v antlr.Token) { s.Op = v }

func (s *MulExprContext) GetLeft() IExprContext { return s.Left }

func (s *MulExprContext) GetRight() IExprContext { return s.Right }

func (s *MulExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *MulExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *MulExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MulExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *MulExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *MulExprContext) STAR() antlr.TerminalNode {
	return s.GetToken(LynxParserSTAR, 0)
}

func (s *MulExprContext) SLASH() antlr.TerminalNode {
	return s.GetToken(LynxParserSLASH, 0)
}

func (s *MulExprContext) PERCENT() antlr.TerminalNode {
	return s.GetToken(LynxParserPERCENT, 0)
}

func (s *MulExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *MulExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterMulExpr(s)
	}
}

func (s *MulExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitMulExpr(s)
	}
}

func (s *MulExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitMulExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type ReassignExprContext struct {
	ExprContext
	Left  IExprContext
	Op    antlr.Token
	Right IExprContext
}

func NewReassignExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ReassignExprContext {
	var p = new(ReassignExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *ReassignExprContext) GetOp() antlr.Token { return s.Op }

func (s *ReassignExprContext) SetOp(v antlr.Token) { s.Op = v }

func (s *ReassignExprContext) GetLeft() IExprContext { return s.Left }

func (s *ReassignExprContext) GetRight() IExprContext { return s.Right }

func (s *ReassignExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *ReassignExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *ReassignExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ReassignExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *ReassignExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *ReassignExprContext) ASSIGN() antlr.TerminalNode {
	return s.GetToken(LynxParserASSIGN, 0)
}

func (s *ReassignExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *ReassignExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterReassignExpr(s)
	}
}

func (s *ReassignExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitReassignExpr(s)
	}
}

func (s *ReassignExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitReassignExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type IfExprContext struct {
	ExprContext
	Cond IExprContext
	Then IStatContext
	Else IStatContext
}

func NewIfExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IfExprContext {
	var p = new(IfExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *IfExprContext) GetCond() IExprContext { return s.Cond }

func (s *IfExprContext) GetThen() IStatContext { return s.Then }

func (s *IfExprContext) GetElse() IStatContext { return s.Else }

func (s *IfExprContext) SetCond(v IExprContext) { s.Cond = v }

func (s *IfExprContext) SetThen(v IStatContext) { s.Then = v }

func (s *IfExprContext) SetElse(v IStatContext) { s.Else = v }

func (s *IfExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfExprContext) IF() antlr.TerminalNode {
	return s.GetToken(LynxParserIF, 0)
}

func (s *IfExprContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *IfExprContext) AllStat() []IStatContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IStatContext); ok {
			len++
		}
	}

	tst := make([]IStatContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IStatContext); ok {
			tst[i] = t.(IStatContext)
			i++
		}
	}

	return tst
}

func (s *IfExprContext) Stat(i int) IStatContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IStatContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IStatContext)
}

func (s *IfExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *IfExprContext) ELSE() antlr.TerminalNode {
	return s.GetToken(LynxParserELSE, 0)
}

func (s *IfExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterIfExpr(s)
	}
}

func (s *IfExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitIfExpr(s)
	}
}

func (s *IfExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitIfExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type TupleExprContext struct {
	ExprContext
}

func NewTupleExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *TupleExprContext {
	var p = new(TupleExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *TupleExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TupleExprContext) LPAREN() antlr.TerminalNode {
	return s.GetToken(LynxParserLPAREN, 0)
}

func (s *TupleExprContext) RPAREN() antlr.TerminalNode {
	return s.GetToken(LynxParserRPAREN, 0)
}

func (s *TupleExprContext) AllNL() []antlr.TerminalNode {
	return s.GetTokens(LynxParserNL)
}

func (s *TupleExprContext) NL(i int) antlr.TerminalNode {
	return s.GetToken(LynxParserNL, i)
}

func (s *TupleExprContext) AllTupleElem() []ITupleElemContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(ITupleElemContext); ok {
			len++
		}
	}

	tst := make([]ITupleElemContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(ITupleElemContext); ok {
			tst[i] = t.(ITupleElemContext)
			i++
		}
	}

	return tst
}

func (s *TupleExprContext) TupleElem(i int) ITupleElemContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITupleElemContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITupleElemContext)
}

func (s *TupleExprContext) AllCOMMA() []antlr.TerminalNode {
	return s.GetTokens(LynxParserCOMMA)
}

func (s *TupleExprContext) COMMA(i int) antlr.TerminalNode {
	return s.GetToken(LynxParserCOMMA, i)
}

func (s *TupleExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterTupleExpr(s)
	}
}

func (s *TupleExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitTupleExpr(s)
	}
}

func (s *TupleExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitTupleExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type PowExprContext struct {
	ExprContext
	Left  IExprContext
	Right IExprContext
}

func NewPowExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *PowExprContext {
	var p = new(PowExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *PowExprContext) GetLeft() IExprContext { return s.Left }

func (s *PowExprContext) GetRight() IExprContext { return s.Right }

func (s *PowExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *PowExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *PowExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PowExprContext) CARET() antlr.TerminalNode {
	return s.GetToken(LynxParserCARET, 0)
}

func (s *PowExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *PowExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *PowExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *PowExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterPowExpr(s)
	}
}

func (s *PowExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitPowExpr(s)
	}
}

func (s *PowExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitPowExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type BitAndExprContext struct {
	ExprContext
	Left  IExprContext
	Right IExprContext
}

func NewBitAndExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BitAndExprContext {
	var p = new(BitAndExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *BitAndExprContext) GetLeft() IExprContext { return s.Left }

func (s *BitAndExprContext) GetRight() IExprContext { return s.Right }

func (s *BitAndExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *BitAndExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *BitAndExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BitAndExprContext) BITAND() antlr.TerminalNode {
	return s.GetToken(LynxParserBITAND, 0)
}

func (s *BitAndExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *BitAndExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *BitAndExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *BitAndExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterBitAndExpr(s)
	}
}

func (s *BitAndExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitBitAndExpr(s)
	}
}

func (s *BitAndExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitBitAndExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type CallExprContext struct {
	ExprContext
	Left IExprContext
}

func NewCallExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *CallExprContext {
	var p = new(CallExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *CallExprContext) GetLeft() IExprContext { return s.Left }

func (s *CallExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *CallExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallExprContext) LPAREN() antlr.TerminalNode {
	return s.GetToken(LynxParserLPAREN, 0)
}

func (s *CallExprContext) RPAREN() antlr.TerminalNode {
	return s.GetToken(LynxParserRPAREN, 0)
}

func (s *CallExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *CallExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *CallExprContext) AllNL() []antlr.TerminalNode {
	return s.GetTokens(LynxParserNL)
}

func (s *CallExprContext) NL(i int) antlr.TerminalNode {
	return s.GetToken(LynxParserNL, i)
}

func (s *CallExprContext) AllCOMMA() []antlr.TerminalNode {
	return s.GetTokens(LynxParserCOMMA)
}

func (s *CallExprContext) COMMA(i int) antlr.TerminalNode {
	return s.GetToken(LynxParserCOMMA, i)
}

func (s *CallExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterCallExpr(s)
	}
}

func (s *CallExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitCallExpr(s)
	}
}

func (s *CallExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitCallExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type IdExprContext struct {
	ExprContext
}

func NewIdExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IdExprContext {
	var p = new(IdExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *IdExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdExprContext) ID() antlr.TerminalNode {
	return s.GetToken(LynxParserID, 0)
}

func (s *IdExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterIdExpr(s)
	}
}

func (s *IdExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitIdExpr(s)
	}
}

func (s *IdExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitIdExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type PointerTypeExprContext struct {
	ExprContext
	Op antlr.Token
}

func NewPointerTypeExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *PointerTypeExprContext {
	var p = new(PointerTypeExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *PointerTypeExprContext) GetOp() antlr.Token { return s.Op }

func (s *PointerTypeExprContext) SetOp(v antlr.Token) { s.Op = v }

func (s *PointerTypeExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PointerTypeExprContext) Expr() IExprContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *PointerTypeExprContext) CARET() antlr.TerminalNode {
	return s.GetToken(LynxParserCARET, 0)
}

func (s *PointerTypeExprContext) VAR() antlr.TerminalNode {
	return s.GetToken(LynxParserVAR, 0)
}

func (s *PointerTypeExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterPointerTypeExpr(s)
	}
}

func (s *PointerTypeExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitPointerTypeExpr(s)
	}
}

func (s *PointerTypeExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitPointerTypeExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type AndExprContext struct {
	ExprContext
	Left  IExprContext
	Right IExprContext
}

func NewAndExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *AndExprContext {
	var p = new(AndExprContext)

	InitEmptyExprContext(&p.ExprContext)
	p.parser = parser
	p.CopyAll(ctx.(*ExprContext))

	return p
}

func (s *AndExprContext) GetLeft() IExprContext { return s.Left }

func (s *AndExprContext) GetRight() IExprContext { return s.Right }

func (s *AndExprContext) SetLeft(v IExprContext) { s.Left = v }

func (s *AndExprContext) SetRight(v IExprContext) { s.Right = v }

func (s *AndExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AndExprContext) AND() antlr.TerminalNode {
	return s.GetToken(LynxParserAND, 0)
}

func (s *AndExprContext) AllExpr() []IExprContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExprContext); ok {
			len++
		}
	}

	tst := make([]IExprContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExprContext); ok {
			tst[i] = t.(IExprContext)
			i++
		}
	}

	return tst
}

func (s *AndExprContext) Expr(i int) IExprContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExprContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *AndExprContext) NL() antlr.TerminalNode {
	return s.GetToken(LynxParserNL, 0)
}

func (s *AndExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.EnterAndExpr(s)
	}
}

func (s *AndExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(LynxParserListener); ok {
		listenerT.ExitAndExpr(s)
	}
}

func (s *AndExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case LynxParserVisitor:
		return t.VisitAndExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *LynxParser) Expr() (localctx IExprContext) {
	return p.expr(0)
}

func (p *LynxParser) expr(_p int) (localctx IExprContext) {
	var _parentctx antlr.ParserRuleContext = p.GetParserRuleContext()

	_parentState := p.GetState()
	localctx = NewExprContext(p, p.GetParserRuleContext(), _parentState)
	var _prevctx IExprContext = localctx
	var _ antlr.ParserRuleContext = _prevctx // TODO: To prevent unused variable warning.
	_startState := 12
	p.EnterRecursionRule(localctx, 12, LynxParserRULE_expr, _p)
	var _la int

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(163)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 29, p.GetParserRuleContext()) {
	case 1:
		localctx = NewNumExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx

		{
			p.SetState(90)
			p.Match(LynxParserNUM)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case 2:
		localctx = NewIdExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(91)
			p.Match(LynxParserID)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case 3:
		localctx = NewParenExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(92)
			p.Match(LynxParserLPAREN)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(94)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserNL {
			{
				p.SetState(93)
				p.Match(LynxParserNL)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(96)
			p.expr(0)
		}
		p.SetState(98)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserNL {
			{
				p.SetState(97)
				p.Match(LynxParserNL)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(100)
			p.Match(LynxParserRPAREN)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case 4:
		localctx = NewMagExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(102)
			p.Match(LynxParserBITOR)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(104)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserNL {
			{
				p.SetState(103)
				p.Match(LynxParserNL)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(106)
			p.expr(0)
		}
		p.SetState(108)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserNL {
			{
				p.SetState(107)
				p.Match(LynxParserNL)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(110)
			p.Match(LynxParserBITOR)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case 5:
		localctx = NewTupleExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(112)
			p.Match(LynxParserLPAREN)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(114)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 19, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(113)
				p.Match(LynxParserNL)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		} else if p.HasError() { // JIM
			goto errorExit
		}
		p.SetState(129)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 22, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(116)
				p.TupleElem()
			}
			{
				p.SetState(117)
				p.Match(LynxParserCOMMA)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		} else if p.HasError() { // JIM
			goto errorExit
		} else if p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 22, p.GetParserRuleContext()) == 2 {
			{
				p.SetState(119)
				p.TupleElem()
			}
			p.SetState(125)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}
			_la = p.GetTokenStream().LA(1)

			for ok := true; ok; ok = _la == LynxParserCOMMA {
				{
					p.SetState(120)
					p.Match(LynxParserCOMMA)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				p.SetState(122)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(121)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(124)
					p.TupleElem()
				}

				p.SetState(127)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)
			}

		} else if p.HasError() { // JIM
			goto errorExit
		}
		p.SetState(132)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserNL {
			{
				p.SetState(131)
				p.Match(LynxParserNL)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(134)
			p.Match(LynxParserRPAREN)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case 6:
		localctx = NewPointerTypeExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(135)

			var _m = p.Match(LynxParserCARET)

			localctx.(*PointerTypeExprContext).Op = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(137)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserVAR {
			{
				p.SetState(136)
				p.Match(LynxParserVAR)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(139)
			p.expr(19)
		}

	case 7:
		localctx = NewAddrExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(140)
			p.Match(LynxParserBITAND)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(142)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserVAR {
			{
				p.SetState(141)
				p.Match(LynxParserVAR)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(144)
			p.expr(15)
		}

	case 8:
		localctx = NewPrefixExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(145)

			var _lt = p.GetTokenStream().LT(1)

			localctx.(*PrefixExprContext).Op = _lt

			_la = p.GetTokenStream().LA(1)

			if !((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&201588736) != 0) {
				var _ri = p.GetErrorHandler().RecoverInline(p)

				localctx.(*PrefixExprContext).Op = _ri
			} else {
				p.GetErrorHandler().ReportMatch(p)
				p.Consume()
			}
		}
		{
			p.SetState(146)
			p.expr(14)
		}

	case 9:
		localctx = NewNotExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(147)
			p.Match(LynxParserNOT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(149)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserNL {
			{
				p.SetState(148)
				p.Match(LynxParserNL)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(151)
			p.expr(6)
		}

	case 10:
		localctx = NewIfExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(152)
			p.Match(LynxParserIF)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(153)

			var _x = p.expr(0)

			localctx.(*IfExprContext).Cond = _x
		}
		p.SetState(155)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == LynxParserNL {
			{
				p.SetState(154)
				p.Match(LynxParserNL)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(157)

			var _x = p.Stat()

			localctx.(*IfExprContext).Then = _x
		}
		p.SetState(160)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 28, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(158)
				p.Match(LynxParserELSE)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}
			{
				p.SetState(159)

				var _x = p.Stat()

				localctx.(*IfExprContext).Else = _x
			}

		} else if p.HasError() { // JIM
			goto errorExit
		}

	case 11:
		localctx = NewMultiStat_Context(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(162)
			p.MultiStat()
		}

	case antlr.ATNInvalidAltNumber:
		goto errorExit
	}
	p.GetParserRuleContext().SetStop(p.GetTokenStream().LT(-1))
	p.SetState(257)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 47, p.GetParserRuleContext())
	if p.HasError() {
		goto errorExit
	}
	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			if p.GetParseListeners() != nil {
				p.TriggerExitRuleEvent()
			}
			_prevctx = localctx
			p.SetState(255)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}

			switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 46, p.GetParserRuleContext()) {
			case 1:
				localctx = NewPowExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*PowExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(165)

				if !(p.Precpred(p.GetParserRuleContext(), 16)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 16)", ""))
					goto errorExit
				}
				{
					p.SetState(166)
					p.Match(LynxParserCARET)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				p.SetState(168)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(167)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(170)

					var _x = p.expr(16)

					localctx.(*PowExprContext).Right = _x
				}

			case 2:
				localctx = NewMulExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*MulExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(171)

				if !(p.Precpred(p.GetParserRuleContext(), 13)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 13)", ""))
					goto errorExit
				}
				{
					p.SetState(172)

					var _lt = p.GetTokenStream().LT(1)

					localctx.(*MulExprContext).Op = _lt

					_la = p.GetTokenStream().LA(1)

					if !((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&29360128) != 0) {
						var _ri = p.GetErrorHandler().RecoverInline(p)

						localctx.(*MulExprContext).Op = _ri
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}
				p.SetState(174)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(173)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(176)

					var _x = p.expr(14)

					localctx.(*MulExprContext).Right = _x
				}

			case 3:
				localctx = NewAddExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*AddExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(177)

				if !(p.Precpred(p.GetParserRuleContext(), 12)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 12)", ""))
					goto errorExit
				}
				{
					p.SetState(178)

					var _lt = p.GetTokenStream().LT(1)

					localctx.(*AddExprContext).Op = _lt

					_la = p.GetTokenStream().LA(1)

					if !(_la == LynxParserPLUS || _la == LynxParserMINUS) {
						var _ri = p.GetErrorHandler().RecoverInline(p)

						localctx.(*AddExprContext).Op = _ri
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}
				p.SetState(180)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(179)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(182)

					var _x = p.expr(13)

					localctx.(*AddExprContext).Right = _x
				}

			case 4:
				localctx = NewShiftExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*ShiftExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(183)

				if !(p.Precpred(p.GetParserRuleContext(), 11)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 11)", ""))
					goto errorExit
				}
				{
					p.SetState(184)

					var _lt = p.GetTokenStream().LT(1)

					localctx.(*ShiftExprContext).Op = _lt

					_la = p.GetTokenStream().LA(1)

					if !(_la == LynxParserLSHIFT || _la == LynxParserRSHIFT) {
						var _ri = p.GetErrorHandler().RecoverInline(p)

						localctx.(*ShiftExprContext).Op = _ri
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}
				p.SetState(186)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(185)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(188)

					var _x = p.expr(12)

					localctx.(*ShiftExprContext).Right = _x
				}

			case 5:
				localctx = NewBitAndExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*BitAndExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(189)

				if !(p.Precpred(p.GetParserRuleContext(), 10)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 10)", ""))
					goto errorExit
				}
				{
					p.SetState(190)
					p.Match(LynxParserBITAND)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				p.SetState(192)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(191)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(194)

					var _x = p.expr(11)

					localctx.(*BitAndExprContext).Right = _x
				}

			case 6:
				localctx = NewBitXorExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*BitXorExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(195)

				if !(p.Precpred(p.GetParserRuleContext(), 9)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 9)", ""))
					goto errorExit
				}
				{
					p.SetState(196)
					p.Match(LynxParserBITXOR)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				p.SetState(198)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(197)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(200)

					var _x = p.expr(10)

					localctx.(*BitXorExprContext).Right = _x
				}

			case 7:
				localctx = NewBitOrExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*BitOrExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(201)

				if !(p.Precpred(p.GetParserRuleContext(), 8)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 8)", ""))
					goto errorExit
				}
				{
					p.SetState(202)
					p.Match(LynxParserBITOR)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				p.SetState(204)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(203)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(206)

					var _x = p.expr(9)

					localctx.(*BitOrExprContext).Right = _x
				}

			case 8:
				localctx = NewCompExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*CompExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(207)

				if !(p.Precpred(p.GetParserRuleContext(), 7)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 7)", ""))
					goto errorExit
				}
				{
					p.SetState(208)

					var _lt = p.GetTokenStream().LT(1)

					localctx.(*CompExprContext).Op = _lt

					_la = p.GetTokenStream().LA(1)

					if !((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&129024) != 0) {
						var _ri = p.GetErrorHandler().RecoverInline(p)

						localctx.(*CompExprContext).Op = _ri
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}
				p.SetState(210)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(209)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(212)

					var _x = p.expr(8)

					localctx.(*CompExprContext).Right = _x
				}

			case 9:
				localctx = NewAndExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*AndExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(213)

				if !(p.Precpred(p.GetParserRuleContext(), 5)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 5)", ""))
					goto errorExit
				}
				{
					p.SetState(214)
					p.Match(LynxParserAND)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				p.SetState(216)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(215)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(218)

					var _x = p.expr(6)

					localctx.(*AndExprContext).Right = _x
				}

			case 10:
				localctx = NewOrExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*OrExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(219)

				if !(p.Precpred(p.GetParserRuleContext(), 4)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 4)", ""))
					goto errorExit
				}
				{
					p.SetState(220)
					p.Match(LynxParserOR)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				p.SetState(222)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(221)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(224)

					var _x = p.expr(5)

					localctx.(*OrExprContext).Right = _x
				}

			case 11:
				localctx = NewReassignExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*ReassignExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(225)

				if !(p.Precpred(p.GetParserRuleContext(), 3)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 3)", ""))
					goto errorExit
				}
				{
					p.SetState(226)

					var _m = p.Match(LynxParserASSIGN)

					localctx.(*ReassignExprContext).Op = _m
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				p.SetState(228)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(227)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				{
					p.SetState(230)

					var _x = p.expr(4)

					localctx.(*ReassignExprContext).Right = _x
				}

			case 12:
				localctx = NewCallExprContext(p, NewExprContext(p, _parentctx, _parentState))
				localctx.(*CallExprContext).Left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(231)

				if !(p.Precpred(p.GetParserRuleContext(), 18)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 18)", ""))
					goto errorExit
				}
				{
					p.SetState(232)
					p.Match(LynxParserLPAREN)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				p.SetState(234)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if _la == LynxParserNL {
					{
						p.SetState(233)
						p.Match(LynxParserNL)
						if p.HasError() {
							// Recognition error - abort rule
							goto errorExit
						}
					}

				}
				p.SetState(250)
				p.GetErrorHandler().Sync(p)
				if p.HasError() {
					goto errorExit
				}
				_la = p.GetTokenStream().LA(1)

				if (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&415237014656) != 0 {
					{
						p.SetState(236)
						p.expr(0)
					}
					p.SetState(244)
					p.GetErrorHandler().Sync(p)
					if p.HasError() {
						goto errorExit
					}
					_la = p.GetTokenStream().LA(1)

					for _la == LynxParserCOMMA {
						{
							p.SetState(237)
							p.Match(LynxParserCOMMA)
							if p.HasError() {
								// Recognition error - abort rule
								goto errorExit
							}
						}
						p.SetState(239)
						p.GetErrorHandler().Sync(p)
						if p.HasError() {
							goto errorExit
						}
						_la = p.GetTokenStream().LA(1)

						if _la == LynxParserNL {
							{
								p.SetState(238)
								p.Match(LynxParserNL)
								if p.HasError() {
									// Recognition error - abort rule
									goto errorExit
								}
							}

						}
						{
							p.SetState(241)
							p.expr(0)
						}

						p.SetState(246)
						p.GetErrorHandler().Sync(p)
						if p.HasError() {
							goto errorExit
						}
						_la = p.GetTokenStream().LA(1)
					}
					p.SetState(248)
					p.GetErrorHandler().Sync(p)
					if p.HasError() {
						goto errorExit
					}
					_la = p.GetTokenStream().LA(1)

					if _la == LynxParserNL {
						{
							p.SetState(247)
							p.Match(LynxParserNL)
							if p.HasError() {
								// Recognition error - abort rule
								goto errorExit
							}
						}

					}

				}
				{
					p.SetState(252)
					p.Match(LynxParserRPAREN)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}

			case 13:
				localctx = NewDerefExprContext(p, NewExprContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, LynxParserRULE_expr)
				p.SetState(253)

				if !(p.Precpred(p.GetParserRuleContext(), 17)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 17)", ""))
					goto errorExit
				}
				{
					p.SetState(254)
					p.Match(LynxParserCARET)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}

			case antlr.ATNInvalidAltNumber:
				goto errorExit
			}

		}
		p.SetState(259)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 47, p.GetParserRuleContext())
		if p.HasError() {
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.UnrollRecursionContexts(_parentctx)
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

func (p *LynxParser) Sempred(localctx antlr.RuleContext, ruleIndex, predIndex int) bool {
	switch ruleIndex {
	case 6:
		var t *ExprContext = nil
		if localctx != nil {
			t = localctx.(*ExprContext)
		}
		return p.Expr_Sempred(t, predIndex)

	default:
		panic("No predicate with index: " + fmt.Sprint(ruleIndex))
	}
}

func (p *LynxParser) Expr_Sempred(localctx antlr.RuleContext, predIndex int) bool {
	switch predIndex {
	case 0:
		return p.Precpred(p.GetParserRuleContext(), 16)

	case 1:
		return p.Precpred(p.GetParserRuleContext(), 13)

	case 2:
		return p.Precpred(p.GetParserRuleContext(), 12)

	case 3:
		return p.Precpred(p.GetParserRuleContext(), 11)

	case 4:
		return p.Precpred(p.GetParserRuleContext(), 10)

	case 5:
		return p.Precpred(p.GetParserRuleContext(), 9)

	case 6:
		return p.Precpred(p.GetParserRuleContext(), 8)

	case 7:
		return p.Precpred(p.GetParserRuleContext(), 7)

	case 8:
		return p.Precpred(p.GetParserRuleContext(), 5)

	case 9:
		return p.Precpred(p.GetParserRuleContext(), 4)

	case 10:
		return p.Precpred(p.GetParserRuleContext(), 3)

	case 11:
		return p.Precpred(p.GetParserRuleContext(), 18)

	case 12:
		return p.Precpred(p.GetParserRuleContext(), 17)

	default:
		panic("No predicate with index: " + fmt.Sprint(predIndex))
	}
}
