// Code generated from LynxParser.g4 by ANTLR 4.13.2. DO NOT EDIT.

package parser // LynxParser

import "github.com/antlr4-go/antlr/v4"

type BaseLynxParserVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseLynxParserVisitor) VisitFileStat(ctx *FileStatContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitEndedStat(ctx *EndedStatContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitLineEnd(ctx *LineEndContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitMultiStat(ctx *MultiStatContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitConstStat(ctx *ConstStatContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitDeclStat(ctx *DeclStatContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitPrintStat(ctx *PrintStatContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitExprStat(ctx *ExprStatContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitTupleElem(ctx *TupleElemContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitMultiStat_(ctx *MultiStat_Context) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitOrExpr(ctx *OrExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitAddrExpr(ctx *AddrExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitNumExpr(ctx *NumExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitParenExpr(ctx *ParenExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitBitXorExpr(ctx *BitXorExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitShiftExpr(ctx *ShiftExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitMagExpr(ctx *MagExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitPrefixExpr(ctx *PrefixExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitBitOrExpr(ctx *BitOrExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitNotExpr(ctx *NotExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitAddExpr(ctx *AddExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitCompExpr(ctx *CompExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitDerefExpr(ctx *DerefExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitMulExpr(ctx *MulExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitReassignExpr(ctx *ReassignExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitIfExpr(ctx *IfExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitTupleExpr(ctx *TupleExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitPowExpr(ctx *PowExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitBitAndExpr(ctx *BitAndExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitCallExpr(ctx *CallExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitIdExpr(ctx *IdExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitPointerTypeExpr(ctx *PointerTypeExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseLynxParserVisitor) VisitAndExpr(ctx *AndExprContext) interface{} {
	return v.VisitChildren(ctx)
}
